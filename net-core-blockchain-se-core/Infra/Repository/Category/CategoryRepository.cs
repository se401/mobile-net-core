﻿using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Category;
using net_core_blockchain_se_core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Repository.Category
{
    public class CategoryRepository : Repository<CategoryDTO>, ICategoryRepository
    {
        private readonly IMongoCollection<CategoryDTO> _categoryCollection;
        public CategoryRepository()
        {
            _categoryCollection = _mongoClient.GetDatabase(DB()).GetCollection<CategoryDTO>(Collection());
        }
        public override string Collection()
        {
            return ConstantCollection.Category_Collection;
        }

        public override string DB()
        {
            return ConstantCollection.DB_Posts;
        }

        public IList<CategoryDTO> GetActiveCategories()
        {
            return _categoryCollection.Find(x=>x.IsActive).ToList();
        }
    }
}
