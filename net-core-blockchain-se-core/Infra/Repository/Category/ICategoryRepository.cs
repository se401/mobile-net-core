﻿using net_core_blockchain_se_core.Infra.Data.DTO.Category;
using net_core_blockchain_se_core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Repository.Category
{
    public interface ICategoryRepository : IRepository<CategoryDTO>
    {
        IList<CategoryDTO> GetActiveCategories();
    }
}
