﻿using MongoDB.Bson;
using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Repository
{
    public interface IRepository<T> where T : BaseDTO
    {
        Task<T> InsertAsync(T entity);
        T Insert(T entity);
        Task<bool> UpdateOneAsync(string id, T entity);
        bool UpdateOne(string id, T entity);
        Task<T> GetByIdAsync(string id);
        T GetById(string id);

        Task<IEnumerable<T>> GetAllAsync();
        IEnumerable<T> GetAll();

        Task InsertManyAsync(IEnumerable<T> entities);
        void InserMany(IEnumerable<T> entities);

    }
}
