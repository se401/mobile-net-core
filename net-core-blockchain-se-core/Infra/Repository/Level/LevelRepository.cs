using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Level;

namespace net_core_blockchain_se_core.Infra.Repository.Level
{
    public class LevelRepository : Repository<LevelDTO>, ILevelRepository
    {
        private readonly IMongoCollection<LevelDTO> _levelColleciton;
        public LevelRepository() : base()
        {
            _levelColleciton = _mongoClient.GetDatabase(DB()).GetCollection<LevelDTO>(Collection());
        }
        public override string Collection()
        {
            return ConstantCollection.Level_Collection;
        }

        public override string DB()
        {
            return ConstantCollection.DB_Posts;
        }

        public IList<LevelDTO> GetActiveLevel()
        {
            return _levelColleciton.Find(x => x.IsActive).ToList();
        }
    }
}