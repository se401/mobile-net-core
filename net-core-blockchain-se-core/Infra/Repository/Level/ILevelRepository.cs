using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Level;

namespace net_core_blockchain_se_core.Infra.Repository.Level
{
    public interface ILevelRepository : IRepository<LevelDTO>
    {
        IList<LevelDTO> GetActiveLevel();
    }
}