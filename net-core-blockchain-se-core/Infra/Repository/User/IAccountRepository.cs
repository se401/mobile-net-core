﻿using net_core_blockchain_se_core.Infra.Data.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Repository.User
{
    public interface IAccountRepository: IRepository<AccountDTO>
    {
        AccountDTO GetByEmail(string email);
        Task<AccountDTO> GetByEmailAsync(string email);
        Task<AccountDTO> GetByWalletAddress(string addr);
    }
}
