﻿using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Repository.User
{
    public class AccountRepository : Repository<AccountDTO>, IAccountRepository
    {
        private readonly IMongoCollection<AccountDTO> _userCollection;
        public AccountRepository():base()
        {
            _userCollection = _mongoClient.GetDatabase(DB()).GetCollection<AccountDTO>(Collection());
            TriggerDb();
        }
        public override string Collection()
        {
            return ConstantCollection.User_Collection;
        }

        public override string DB()
        {
            return ConstantCollection.DB_User;
        }

        public AccountDTO GetByEmail(string email)
        {
            return _userCollection.Find(x => x.Email == email).FirstOrDefault();
        }

        public async Task<AccountDTO> GetByEmailAsync(string email)
        {
            return await _userCollection.Find(x => x.Email == email).FirstOrDefaultAsync();
        }

        public async Task<AccountDTO> GetByWalletAddress(string addr)
        {
            return await _userCollection.Find(x => x.WalletAddress == addr).FirstOrDefaultAsync();
        }

        private void TriggerDb()
        {
            var option = new CreateIndexOptions { Unique = true };
            var listIdx = new List<IndexKeysDefinition<AccountDTO>>()
            {
                new IndexKeysDefinitionBuilder<AccountDTO>().Ascending(x=>x.Email),
                new IndexKeysDefinitionBuilder<AccountDTO>().Ascending(x=>x.Username),
                new IndexKeysDefinitionBuilder<AccountDTO>().Ascending(x=>x.PhoneNumber)
            };

            var listUserIndexModel = new List<CreateIndexModel<AccountDTO>>();
            listIdx.ForEach(definition =>
            {
                listUserIndexModel.Add(new CreateIndexModel<AccountDTO>(definition, option));
            });
            _userCollection.Indexes.CreateMany(listUserIndexModel);
        }
    }
}
