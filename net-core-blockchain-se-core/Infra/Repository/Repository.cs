﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using net_core_blockchain_se_core.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : BaseDTO
    {
        protected readonly IMongoClient _mongoClient;
        private readonly IMongoCollection<T> _collection;
        public Repository()
        {
            var rootConfiguration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            string connectionStr = rootConfiguration.GetSection("Mongo:Uri").Value.ToString();
            _mongoClient = new MongoClient(connectionStr);
            _collection = _mongoClient.GetDatabase(DB()).GetCollection<T>(Collection());
        }

        public abstract string DB();
        public abstract string Collection();
        public IEnumerable<T> GetAll()
        {
            return _collection.Find(Builders<T>.Filter.Empty).ToEnumerable();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return (await _collection.FindAsync(Builders<T>.Filter.Empty)).ToEnumerable();
        }

        public T GetById(string id)
        {
            return _collection.Find(Builders<T>.Filter.Eq("_id", ObjectId.Parse(id))).FirstOrDefault();
        }

        public async Task<T> GetByIdAsync(string id)
        {
            return (await _collection.FindAsync(Builders<T>.Filter.Eq("_id", ObjectId.Parse(id)))).FirstOrDefault();
        }

        public T Insert(T entity)
        {
            _collection.InsertOne(entity);
            return entity;
        }

        public async Task<T> InsertAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
            return entity;
        }

        public bool UpdateOne(string id, T entity)
        {
            var result = _collection.ReplaceOne(Builders<T>.Filter.Eq("_id", ObjectId.Parse(id)), entity);
            return result.ModifiedCount > 0;
        }

        public async Task<bool> UpdateOneAsync(string id, T entity)
        {
            var result = await _collection.ReplaceOneAsync(Builders<T>.Filter.Eq("_id", ObjectId.Parse(id)), entity);


            return result.ModifiedCount > 0;
        }

        public async Task InsertManyAsync(IEnumerable<T> entities)
        {
            await _collection.InsertManyAsync(entities);
        }

        public void InserMany(IEnumerable<T> entities)
        {
            _collection.InsertMany(entities);
        }
    }
}
