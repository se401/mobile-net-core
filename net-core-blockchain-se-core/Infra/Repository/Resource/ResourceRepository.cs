using System.Collections.Generic;
using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Resource;

namespace net_core_blockchain_se_core.Infra.Repository.Resource
{
    public class ResourceRepository : Repository<ResourceDTO>, IResourceRepository
    {
        private readonly IMongoCollection<ResourceDTO> _resourceCollection;
        public ResourceRepository():base()
        {
            _resourceCollection = _mongoClient.GetDatabase(DB()).GetCollection<ResourceDTO>(Collection());
        }
        public override string Collection()
        {
            return ConstantCollection.Resource_Collection;
        }

        public override string DB()
        {
            return ConstantCollection.DB_Posts;
        }

        public IList<ResourceDTO> GetActiveResource()
        {
            return _resourceCollection.Find(x=>x.IsActive).ToList();
        }
    }
}