using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Resource;

namespace net_core_blockchain_se_core.Infra.Repository.Resource
{
    public interface IResourceRepository:IRepository<ResourceDTO>
    {
        IList<ResourceDTO> GetActiveResource();
    }
}