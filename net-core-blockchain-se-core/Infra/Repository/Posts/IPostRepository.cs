using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Post;

namespace net_core_blockchain_se_core.Infra.Repository.Posts
{
    public interface IPostRepository:IRepository<PostDTO>
    {
        IEnumerable<PostDTO> GetActivePosts();
        IEnumerable<PostDTO> GetPostWithCategory(string categoryId);
        IEnumerable<PostDTO> GetPostByResource(string resourceId);
        IEnumerable<PostDTO> GetPostByAuthor(string authorId);
        IEnumerable<PostDTO> GetApprovedPosts();
    }
}