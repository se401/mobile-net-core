using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Post;

namespace net_core_blockchain_se_core.Infra.Repository.Posts
{
    public class PostRepository : Repository<PostDTO>, IPostRepository
    {
        private readonly IMongoCollection<PostDTO> _postCollection;
        public PostRepository() : base()
        {
            _postCollection = _mongoClient.GetDatabase(DB()).GetCollection<PostDTO>(Collection());
        }
        public override string Collection()
        {
            return ConstantCollection.Post_Collection;
        }

        public override string DB()
        {
            return ConstantCollection.DB_Posts;
        }

        public IEnumerable<PostDTO> GetActivePosts()
        {
            return _postCollection.Find(x => x.IsActive).ToEnumerable();
        }

        public IEnumerable<PostDTO> GetApprovedPosts()
        {
            return _postCollection.Find(x=>x.Status == Enums.PostState.COMPLETE).ToEnumerable();
        }

        public IEnumerable<PostDTO> GetPostByAuthor(string authorId)
        {
            return _postCollection.Find(x => x.Author._id.Equals(ObjectId.Parse(authorId))).ToEnumerable();
        }

        public IEnumerable<PostDTO> GetPostByResource(string resourceId)
        {
            return _postCollection.Find(x => x.Resource._id.Equals(ObjectId.Parse(resourceId))).ToEnumerable();
        }

        public IEnumerable<PostDTO> GetPostWithCategory(string categoryId)
        {
            return _postCollection.Find(x => x.Category._id.Equals(ObjectId.Parse(categoryId))).ToEnumerable();
        }

    }
}