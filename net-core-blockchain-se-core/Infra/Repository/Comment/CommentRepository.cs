﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Comment;

namespace net_core_blockchain_se_core.Infra.Repository.Comment
{
    public class CommentRepository:Repository<CommentDTO>, ICommentRepository
    {
        private readonly IMongoCollection<CommentDTO> _commentCollection;
        public CommentRepository():base()
        {
            _commentCollection = _mongoClient.GetDatabase(DB()).GetCollection<CommentDTO>(Collection());
        }
        public override string DB()
        {
            return ConstantCollection.DB_Posts;
        }

        public override string Collection()
        {
            return ConstantCollection.Commet_Collection;
        }

        public IEnumerable<CommentDTO> GetCommentByPost(string postId)
        {
            return _commentCollection.Find(x => x.PostId == postId).ToEnumerable();
        }

        public IEnumerable<CommentDTO> GetActiveCommentByPost(string postId)
        {
            return _commentCollection.Find(x => x.PostId == postId && x.IsActive && !x.IsReported).ToEnumerable();
        }

        public IEnumerable<CommentDTO> GetCommentByUser(string userId)
        {
            return _commentCollection.Find(x => x.User._id.Equals(ObjectId.Parse(userId))).ToEnumerable();
        }
    }
}