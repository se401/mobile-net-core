﻿using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Comment;

namespace net_core_blockchain_se_core.Infra.Repository.Comment
{
    public interface ICommentRepository:IRepository<CommentDTO>
    {
        IEnumerable<CommentDTO> GetCommentByPost(string postId);
        IEnumerable<CommentDTO> GetActiveCommentByPost(string postId);

        IEnumerable<CommentDTO> GetCommentByUser(string userId);
    }
}