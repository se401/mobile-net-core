﻿using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Transaction;

namespace net_core_blockchain_se_core.Infra.Repository.Transaction
{
    public interface ITransactionRepository:IRepository<TransactionDTO>
    {
        IList<TransactionDTO> GetTransactionByUser(string userId);
    }
}