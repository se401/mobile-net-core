﻿using System.Collections.Generic;
using MongoDB.Driver;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Transaction;

namespace net_core_blockchain_se_core.Infra.Repository.Transaction
{
    public class TransactionRepository:Repository<TransactionDTO>,ITransactionRepository
    {
        private IMongoCollection<TransactionDTO> _transactionCollection;

        public TransactionRepository()
        {
            _transactionCollection = _mongoClient.GetDatabase(DB()).GetCollection<TransactionDTO>(Collection());
        }
        public override string DB()
        {
            return ConstantCollection.DB_User;
        }

        public override string Collection()
        {
            return ConstantCollection.Transaction_Collection;
        }

        public IList<TransactionDTO> GetTransactionByUser(string userId)
        {
            return _transactionCollection.Find(x => x.UserId == userId).ToList();
        }
    }
}