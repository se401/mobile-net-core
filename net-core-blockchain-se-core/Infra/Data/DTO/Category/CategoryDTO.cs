﻿using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Infra.Data.DTO.Category
{
    public class CategoryDTO : BaseDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumnailUrl { get; set; }
        public CategoryDTO Parent { get; set; } = null;
    }
}
