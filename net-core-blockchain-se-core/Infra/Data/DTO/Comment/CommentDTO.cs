﻿using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using net_core_blockchain_se_core.Infra.Data.DTO.User;

namespace net_core_blockchain_se_core.Infra.Data.DTO.Comment
{
    public class CommentDTO : BaseDTO
    {
        public string Text { get; set; }
        public CommentDTO ReplyTo { get; set; }
        public string PostId { get; set; }
        public bool IsReported { get; set; }
        public IList<Report> Reports { get; set; }
        public AccountDTO User { get; set; }

        public CommentDTO()
        {
            Reports = new List<Report>();
        }
    }

    public class Report
    {
        public AccountDTO User { get; set; }
        public List<string> Reasons { get; set; }
    }
}