using net_core_blockchain_se_core.Infra.Data.DTO.Base;

namespace net_core_blockchain_se_core.Infra.Data.DTO.Level
{
    public class LevelDTO:BaseDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}