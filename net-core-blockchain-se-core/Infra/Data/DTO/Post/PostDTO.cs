using System;
using System.Collections.Generic;
using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using net_core_blockchain_se_core.Infra.Data.DTO.Category;
using net_core_blockchain_se_core.Infra.Data.DTO.Level;
using net_core_blockchain_se_core.Infra.Data.DTO.Resource;
using net_core_blockchain_se_core.Infra.Data.DTO.User;
using static net_core_blockchain_se_core.Constant.Enums;

namespace net_core_blockchain_se_core.Infra.Data.DTO.Post
{
    public class PostDTO : BaseDTO
    {
        public String Title { get; set; }
        public String BackgroudUrl { get; set; }
        public String Description { get; set; }
        public int ViewCount { get; set; }
        public PostState Status { get; set; } = PostState.PENDING_APPROVE;
        public AccountDTO Author { get; set; }
        public CategoryDTO Category { get; set; }
        public LevelDTO Level { get; set; }
        public ResourceDTO Resource { get; set; }
        public int LikeCount { get; set; }
        public String Url { get; set; }
        public List<ReportPostModel> Reports { get; set; }
        public List<EditHistory> EditHistories { get; set; }
        public Decimal Price { get; set; }
        public PostDTO()
        {
            Reports = new List<ReportPostModel>();
            EditHistories = new List<EditHistory>();
        }
    }

    public class ReportPostModel
    {
        public AccountDTO Reporter { get; set; }
        public List<String> Reason { get; set; }
    }

    public class EditHistory
    {
        public AccountDTO Editor { get; set; }
        public PostDTO CurrentData { get; set; }
        public PostDTO NewData { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}