﻿using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Infra.Data.DTO.Base;

namespace net_core_blockchain_se_core.Infra.Data.DTO.Transaction
{
    public class TransactionDTO:BaseDTO
    {
        public string UserId { get; set; }
        public string FromWalletAddress { get; set; }
        public decimal TransactionAmount { get; set; }
        public ConstantCollection.TransactionType TransactionType { get; set; }
        public string ToWalletAddress { get; set; }
        public string Description { get; set; }
    }
}