﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace net_core_blockchain_se_core.Infra.Data.DTO.Base
{
    public class BaseDTO
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public long Timestamp { get; set; } = DateTime.Now.Ticks;

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastModifiedDate { get; set; }
        public bool IsActive { get; set; } = true;
        public string Domain { get; set; }
    }
}
