﻿using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using System;
using System.Collections.Generic;

namespace net_core_blockchain_se_core.Infra.Data.DTO.User
{
    public class AccountDTO : BaseDTO
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string DisplayName { get; set; }
        
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public string Role { get; set; }
        public string VerificationCode { get; set; }
        public DateTime LastAccessedDate { get; set; }
        public string AvatarUrl { get; set; }
        public IList<string> Bookmarks { get; set; }
        public IList<string> LikedPosts { get; set; }
        public IList<string> PaidPosts { get; set; }
        public List<ApiKey> ApiKeys { get; set; }
        public String WalletAddress { get; set; }

        public AccountDTO()
        {
            LikedPosts = new List<string>();
            PaidPosts = new List<string>();
            Bookmarks = new List<string>();
            ApiKeys = new List<ApiKey>();
        }
        
    }
    public class ApiKey
    {
        public string Key { get; set; }
        public string[] Privileges { get; set; }
    }
    
}
