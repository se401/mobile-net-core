﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Utility.Sendgrid
{
    public class SendgridUtil
    {
        private static SendgridUtil instance;
        private SendGridClient client;
        public static SendgridUtil GetInstance()
        {
            if (instance == null)
                instance = new SendgridUtil();
            return instance;
        }

        private SendgridUtil()
        {
            client = new SendGridClient(key);
        }
        private const string key = "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";
        public const string no_reply = "no-reply@cocoduc.ml";

        public async Task SendEmail(string fromEmail, string toEmail, string subject, string htmlContent)
        {
            var from = new EmailAddress(fromEmail);
            var to = new EmailAddress(toEmail);

            var msg = MailHelper.CreateSingleEmail(from, to, subject, String.Empty, htmlContent);
            await client.SendEmailAsync(msg);
        }
    }
}
