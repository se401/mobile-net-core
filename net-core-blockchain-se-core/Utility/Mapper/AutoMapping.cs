﻿using AutoMapper;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Domain.Authentication.Resps;
using net_core_blockchain_se_core.Domain.Category.Resps;
using net_core_blockchain_se_core.Domain.Level.Resps;
using net_core_blockchain_se_core.Domain.Post.Resps;
using net_core_blockchain_se_core.Domain.Resource.Resps;
using net_core_blockchain_se_core.Infra.Data.DTO.Base;
using net_core_blockchain_se_core.Infra.Data.DTO.Category;
using net_core_blockchain_se_core.Infra.Data.DTO.Level;
using net_core_blockchain_se_core.Infra.Data.DTO.Post;
using net_core_blockchain_se_core.Infra.Data.DTO.Resource;
using net_core_blockchain_se_core.Infra.Data.DTO.User;
using net_core_blockchain_se_core.Model;
using System;
using net_core_blockchain_se_core.Domain.Comment.Resp;
using net_core_blockchain_se_core.Domain.User.Resps;
using net_core_blockchain_se_core.Infra.Data.DTO.Comment;
using net_core_blockchain_se_core.Infra.Data.DTO.Transaction;

namespace net_core_blockchain_se_core.Utility.Mapper
{
    public class AutoMapping : Profile
    {

        public AutoMapping()
        {
            CreateMap<AccountDTO, AccountResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<CategoryDTO, CategoryResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<BaseDTO, BaseResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<ResourceDTO, ResourceResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<LevelDTO, LevelResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<PostDTO, PostResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<CommentDTO,CommentResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            CreateMap<AccountDTO,UserResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            ;
            CreateMap<TransactionDTO,TransactionResp>().ForMember(x => x.Id, opt => opt.MapFrom(x => x._id.ToString()));
            ;
        }
    }

    public static class Mapping
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(conf =>
            {
                conf.AllowNullCollections = true;
                conf.AddProfile<AutoMapping>();

            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }
}
