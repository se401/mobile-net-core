﻿using System;
using System.Collections.Generic;
using net_core_blockchain_se_core.Domain.Util.Commands;

namespace net_core_blockchain_se_core.Utility.Others
{
    public class ConvertUtil
    {
        private static ConvertUtil instance;
        private static Dictionary<string, double> defaultConversion = new Dictionary<string, double>();

        public static ConvertUtil Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConvertUtil();
                }

                return instance;
            }
            set => instance = value;
        }

        public ConvertUtil()
        {
            defaultConversion.Add("USD",0.15);
            defaultConversion.Add("VND",3300);
        }

        public Tuple<decimal,string> Convert(CommandRequestConversion cmd)
        {
            try
            {
                var currency = defaultConversion[cmd.SourceCurrency];
                var convertAmount = cmd.Amount / (Decimal) currency;
                return new Tuple<decimal, string>(convertAmount, String.Empty);
            }
            catch (Exception e)
            {
                return new Tuple<decimal, string>(0, "Invalid source currency");
            }
            
        }
    }
}