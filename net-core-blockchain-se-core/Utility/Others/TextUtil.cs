﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Utility.Others
{
    public class TextUtil
    {
        const string digits = "0123456789";
        const string chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        static Random rand = new Random();
        public static string GenerateRandomCode(int length)
        {
            return new string(Enumerable.Repeat(digits, length)
              .Select(s => s[rand.Next(s.Length)]).ToArray());
        }

        public static string GenerateRandomText(int length)
        {
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[rand.Next(s.Length)]).ToArray());
        }
    }
}
