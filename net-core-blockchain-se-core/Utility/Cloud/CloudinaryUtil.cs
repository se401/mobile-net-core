using System;
using System.IO;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace net_core_blockchain_se_core.Utility.Cloud
{
    public class CloudinaryUtil
    {
        private readonly Cloudinary _cloudinary;

        private static CloudinaryUtil instance;

        public static CloudinaryUtil getInstance
        {
            get
            {
                if (instance == null)
                    instance = new CloudinaryUtil();
                return instance;
            }
        }

        public CloudinaryUtil()
        {
            Account account = new Account
            {
                ApiKey = "417944196743641",
                ApiSecret = "sUFvYYRG3d1iPl0QI-XWxDowank",
                Cloud = "dzbsixzpp"
            };
            _cloudinary = new Cloudinary(account);
        }

        public string UploadImageToCloudinary(string data, string folderPath)
        {
            try
            {

                string imgName = Guid.NewGuid().ToString();
                var uploadParams = new ImageUploadParams
                {
                    File = new FileDescription(@"data:image/png;base64," + data + ""),
                    Folder = folderPath
                };
                var uploadResult = _cloudinary.Upload(uploadParams);
                return uploadResult.SecureUri.AbsoluteUri;
            }
            catch (Exception e)
            {
                return String.Empty;
            }
        }

        public string UploadFileToCloudinary(Stream stream, string folderPath)
        {
            try
            {

                string imgName = Guid.NewGuid().ToString();
                var uploadParams = new RawUploadParams
                {
                    File = new FileDescription(imgName,stream),
                    Folder = folderPath
                };
                var uploadResult = _cloudinary.Upload(uploadParams);
                return uploadResult.SecureUri.AbsoluteUri;
            }
            catch (Exception e)
            {
                
                return "99,"+e.Message;
            }
        }

    }
}