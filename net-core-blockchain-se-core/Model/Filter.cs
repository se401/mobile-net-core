﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static net_core_blockchain_se_core.Constant.Enums;

namespace net_core_blockchain_se_core.Model
{
    public class Filter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; } = 1;
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string FilterField { get; set; }
        public string FilterOperator { get; set; }
        public string FilterValue { get; set; }
    }
}
