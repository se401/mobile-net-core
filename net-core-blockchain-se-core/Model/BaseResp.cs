﻿using System;

namespace net_core_blockchain_se_core.Model
{
    public class BaseResp
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public long Timestamp { get; set; }
        public bool IsActive { get; set; }
        public string Domain { get; set; }
    }
}
