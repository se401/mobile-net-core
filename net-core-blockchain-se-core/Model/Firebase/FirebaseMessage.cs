﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace net_core_blockchain_se_core.Model.Firebase
{
    public class FirebaseMessage
    {
        public string[] registration_ids { get; set; }
        public Notification notification { get; set; }
        public object data { get; set; }

        public FirebaseMessage(string[] clientToken, Notification notification, object data)
        {
            this.registration_ids = clientToken;
            this.notification = notification;
            this.data = data;
        }
    }
    public class Notification
    {
        public string title { get; set; }
        public string text { get; set; }
    }
}