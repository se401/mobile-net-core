﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using net_core_blockchain_se_core.Extensions;

namespace net_core_blockchain_se_core.Model
{
    public class Resp<T>
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public T Payload { get; set; }
        public Filter Filter { get; set; } = null;
        public Paging Paging { get; set; }

        public Resp(HttpStatusCode statusCode, string message, T payload, Filter filter = null, Paging paging = null)
        {
            this.StatusCode = statusCode;
            this.Message = message;
            this.Payload = payload;
            this.Filter = filter;
            this.Paging = paging;
        }
    }
}
