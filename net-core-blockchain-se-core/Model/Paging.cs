using System;
using System.Collections.Generic;
using System.Linq;

namespace net_core_blockchain_se_core.Model
{
    public class Paging
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int Total { get; set; }
        public int TotalPages { get; set; }
        public Paging(int pageSize, int pageNumber, int total)
        {
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            this.Total = total;
            this.TotalPages = (int)Math.Ceiling(total / (double)pageSize);
        }

        public static Paging ToPaging<T>(Filter filter, IEnumerable<T> source)
        {
            if (filter.PageSize == 0)
                filter.PageSize = source.Count();
            return new Paging(filter.PageSize, filter.PageNumber, source.Count());
        }
    }
}