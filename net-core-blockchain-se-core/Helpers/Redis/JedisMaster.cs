﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Helpers.Redis
{
    public class JedisMaster : IJedisMaster
    {
        private readonly ConnectionMultiplexer _redis;
        public JedisMaster()
        {

            var rootConfiguration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            string host = rootConfiguration.GetSection("Jedis:Host").Value.ToString();
            int port = Convert.ToInt32(rootConfiguration.GetSection("Jedis:Port").Value.ToString());
            string password = rootConfiguration.GetSection("Jedis:Password").Value.ToString();

            ConfigurationOptions config = new ConfigurationOptions
            {
                ConnectRetry = 30,
                ConnectTimeout = 30000,
                AllowAdmin = true,
                Password = password
            };
            config.EndPoints.Add(host, port);
            _redis = ConnectionMultiplexer.Connect(config);
        }
        public void DeleteKey(int numdb, string key)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                db.KeyDelete(key);
            }
            catch (Exception)
            {

            }
        }

        public async Task DeleteKeyAsync(int numdb, string key)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                await db.KeyDeleteAsync(key);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetViewCount(int numdb, string hkey, string field)
        {
            int count = 0;
            IDatabase db = _redis.GetDatabase(numdb, false);
            string value = db.HashGet(hkey, field);
            Int32.TryParse(value, out count);
            return count;

        }

        public async Task IncrHash(int numdb, string hkey, string field)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb, false);
                await db.HashIncrementAsync(hkey, field);
            }
            catch (System.Exception)
            {

            }
        }

        public string Pull(int numdb, string key)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                return db.StringGet(key);
            }
            catch (Exception)
            {

                return String.Empty;
            }
        }

        public async Task<string> PullAsync(int numdb, string key)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                return await db.StringGetAsync(key);
            }
            catch (Exception)
            {

                return String.Empty;
            }
        }

        public HashEntry[] PullHash(int numdb, string hKey)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                return db.HashGetAll(hKey);
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async Task<HashEntry[]> PullHashAsync(int numdb, string hKey)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                return await db.HashGetAllAsync(hKey);
            }
            catch (Exception)
            {

                return null;
            }
        }

        public string PullSpecificHash(int numdb, string hKey, string field)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                return db.HashGet(hKey, field);
            }
            catch (Exception)
            {

                return String.Empty;
            }
        }

        public async Task<string> PullSpecificHashAsync(int numdb, string hKey, string field)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                return await db.HashGetAsync(hKey, field);
            }
            catch (Exception)
            {

                return String.Empty;
            }
        }

        public void Push(int numdb, string key, string value, TimeSpan? expiry)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                db.StringSet(key, value, expiry);
            }
            catch (Exception)
            {

            }
        }

        public async Task PushAsync(int numdb, string key, string value, TimeSpan? expiry)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                await db.StringSetAsync(key, value, expiry);
            }
            catch (Exception)
            {

            }
        }

        public void PushHash(int numdb, string hKey, string field, string value)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                db.HashSet(hKey, field, value);
            }
            catch (Exception)
            {


            }
        }

        public async Task PushHashAsync(int numdb, string hKey, string field, string value)
        {
            try
            {
                IDatabase db = _redis.GetDatabase(numdb);
                await db.HashSetAsync(hKey, field, value);
            }
            catch (Exception)
            {


            }
        }
    }
}
