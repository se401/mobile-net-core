﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Helpers.Redis
{
     public interface IJedisMaster
     {
          public void Push(int numdb, string key, string value, TimeSpan? expiry);
          public Task PushAsync(int numdb, string key, string value, TimeSpan? expiry);

          public string Pull(int numdb, string key);
          public Task<string> PullAsync(int numdb, string key);

          public void PushHash(int numdb, string hKey, string field, string value);
          public Task PushHashAsync(int numdb, string hKey, string field, string value);
          public HashEntry[] PullHash(int numdb, string hKey);
          public Task<HashEntry[]> PullHashAsync(int numdb, string hKey);

          public string PullSpecificHash(int numdb, string hKey, string field);
          public Task<string> PullSpecificHashAsync(int numdb, string hKey, string field);
          public void DeleteKey(int numdb, string key);
          public Task DeleteKeyAsync(int numdb, string key);

          public Task IncrHash(int numdb, string hkey, string field);

          int GetViewCount(int numdb, string hkey, string field);
     }
}
