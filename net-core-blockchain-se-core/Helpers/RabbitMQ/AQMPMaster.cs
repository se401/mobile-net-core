using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using net_core_blockchain_se_core.Domain.Post.Resps;
using net_core_blockchain_se_core.Infra.Data.DTO.Post;
using net_core_blockchain_se_core.Infra.Repository.Posts;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace net_core_blockchain_se_core.Helpers.RabbitMQ
{
    public class AQMPMaster : IAQMPMaster
    {
        private IModel modelPublish;
        private IModel modelPostUpdate;
        private readonly IConnection connection;
        private IConfiguration _configuration;

        private readonly IPostRepository _postRepository;
        private string QUEUE_UPDATE_POST;


        public AQMPMaster(IConfiguration configuration,
                        IPostRepository postRepository)
        {
            _postRepository = postRepository;
            _configuration = configuration;
            QUEUE_UPDATE_POST = _configuration.GetSection("AQMP:QUEUE_UPDATE_POST").Value.ToString();
            connection = createConnection();
            InitModel();
        }
        public void Publish(string routingQueue, string data)
        {
            try
            {
                IBasicProperties basicProperties = modelPublish.CreateBasicProperties();
                basicProperties.Persistent = true;
                modelPublish.BasicPublish("", routingQueue, false, basicProperties,Encoding.UTF8.GetBytes(data));
            }
            catch (System.Exception)
            {

            }
        }

        private IConnection createConnection()
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.HostName ="gerbil.rmq.cloudamqp.com";
            factory.UserName = "zikysfxh";
            factory.Password = "wCktfMhk7TqxuJUF5N1X2Gq641qsnzFU";
            factory.VirtualHost ="zikysfxh";
            factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);
            factory.ContinuationTimeout = TimeSpan.FromSeconds(30);
            return factory.CreateConnection();
        }
        private void InitModel()
        {
            modelPublish = this.connection.CreateModel();
            modelPublish.BasicQos(0, 1, false);

            //consumer
            modelPostUpdate = this.connection.CreateModel();
            modelPostUpdate.QueueDeclare(QUEUE_UPDATE_POST, true, false, false, null);
            modelPostUpdate.BasicQos(0, 1, false);
            var consumerUpdate = new EventingBasicConsumer(modelPostUpdate);
            consumerUpdate.Received += (model, ea) =>
            {
                var body = ea.Body;
                string message = Encoding.UTF8.GetString(body.ToArray());
                UpdatePost(message);
                modelPostUpdate.BasicAck(ea.DeliveryTag, false);

            };
            modelPostUpdate.BasicConsume(QUEUE_UPDATE_POST, false, consumerUpdate);
        }

        private void UpdatePost(string message)
        {
            try
            {
                var post = JsonConvert.DeserializeObject<PostResp>(message);
                var postInDb = _postRepository.GetById(post.Id);
                postInDb.ViewCount = post.ViewCount;
                _postRepository.UpdateOne(postInDb._id.ToString(), postInDb);
            }
            catch(Exception e)
            {

            }
        }
    }
}