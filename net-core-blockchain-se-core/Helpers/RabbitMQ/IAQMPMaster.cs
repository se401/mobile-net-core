using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Helpers.RabbitMQ
{
    public interface IAQMPMaster
    {
         void Publish(string routingQueue, string data);
    }
}