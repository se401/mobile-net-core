﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Helpers.Firebase
{
    public interface IFirebaseMaster
    {
        Task SendNotification(List<String> clientToken,String title, String message);
    }
}