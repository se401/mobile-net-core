﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.Configuration;
using net_core_blockchain_se_core.Model.Firebase;
using Notification = net_core_blockchain_se_core.Model.Firebase.Notification;

namespace net_core_blockchain_se_core.Helpers.Firebase
{
    public class FirebaseMaster:IFirebaseMaster
    {
        private readonly IConfiguration _configuration;

        public FirebaseMaster(IConfiguration configuration)
        {
            _configuration = configuration;
            Authentication();
        }

        private void Authentication()
        {
            var path = Directory.GetCurrentDirectory();
            string fileName = "firebase.json";
            var fullPath = Path.Combine(path, fileName);
            // var credential = _configuration.GetSection("Firebase:Credential");
            // var credentialInfo = credential.ToString();

            var authInfomation = GoogleCredential.FromFile(fullPath);
            FirebaseApp.Create(new AppOptions()
            {
                Credential = authInfomation
            });

            string[] tokens = new[] {"273690410205"};
            FirebaseMessaging.DefaultInstance.SubscribeToTopicAsync(tokens, "test");
        }

        public async Task SendNotification(List<string> clientToken, string title, string message)
        {
            
            string[] tokens = new[] {"273690410205"};
            MulticastMessage multicastMessage = new MulticastMessage()
            {
                Tokens =  tokens,
                Notification =  new FirebaseAdmin.Messaging.Notification()
                {
                    Title =  title,
                    Body =  message
                }
            };
            var response =  await FirebaseMessaging.DefaultInstance.SendMulticastAsync(multicastMessage);
        }
    }
}