﻿using System;
using System.Threading.Tasks;
using net_core_blockchain_se_core.Helpers.Ethereum.EthModels;

namespace net_core_blockchain_se_core.Helpers.Ethereum
{
    public interface IEthHelpers
    {
        Task<CreateAccountResp> CreateEthAccount(CreateAccountModel createAccountModel);
        Task<Tuple<bool,string,string>> GetLoginToken(CreateAccountModel loginModel);
        Task<String> GetApiKey(GetApiKeyModel getApiKeyModel,String loginToken);
        Task<Tuple<bool,string,string>> SendEth(SendEthModel sendEthModel,String loginToken, String apiKey = null);
        Task<String> GetEthBalance(String loginToken, String apiKey = null);
        Task<String> GenerateApiKey(String loginToken, CreateApiKey model);
        T Decode<T>(String source);
    }
}