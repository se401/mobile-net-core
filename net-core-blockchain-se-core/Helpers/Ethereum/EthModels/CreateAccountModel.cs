﻿using System;
using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Helpers.Ethereum.EthModels
{
    public class CreateAccountModel
    {
        public String email { get; set; }
        public String password { get; set; }

        public CreateAccountModel(String email, String password)
        {
            this.email = email;
            this.password = password;
        }
    }
}