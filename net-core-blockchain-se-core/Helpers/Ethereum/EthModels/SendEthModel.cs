﻿using System;

namespace net_core_blockchain_se_core.Helpers.Ethereum.EthModels
{
    public class SendEthModel
    {
        public String fromAddr { get; set; }
        public String toAddr { get; set; }
        public String amount { get; set; }
    }
}