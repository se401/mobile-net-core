﻿using System;

namespace net_core_blockchain_se_core.Helpers.Ethereum.EthModels
{
    public class GetApiKeyModel
    {
        public String LoginToken { get; set; }
    }
}