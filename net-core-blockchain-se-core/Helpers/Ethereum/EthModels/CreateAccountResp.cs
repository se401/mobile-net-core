﻿using System;
using System.Collections.Generic;

namespace net_core_blockchain_se_core.Helpers.Ethereum.EthModels
{
    public class CreateAccountResp
    {
        public List<String> PublicKey { get; set; }
        public bool IsActive { get; set; }
        public List<Api> ApiKeys { get; set; }
        public String Address { get; set; }
        public int Code { get; set; }
        public String Error { get; set; }
    }

    public class Api
    {
        public String Key { get; set; }
        public String Role { get; set; }
    }
}