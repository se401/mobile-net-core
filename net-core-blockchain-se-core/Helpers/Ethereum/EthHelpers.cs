﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson.IO;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Helpers.Ethereum.EthModels;
using net_core_blockchain_se_core.Helpers.Http;
using net_core_blockchain_se_core.Helpers.Redis;
using Newtonsoft.Json.Linq;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace net_core_blockchain_se_core.Helpers.Ethereum
{
    public class EthHelpers:IEthHelpers
    {
        private readonly IConfiguration _configuration;
        private readonly string BaseUrl;
        private readonly IHttpHelpers _httpHelpers;

        public EthHelpers(IConfiguration configuration,
                            IHttpHelpers httpHelpers)
        {
            _configuration = configuration;
            BaseUrl = _configuration.GetSection("BLOCKCHAIN:URL").Value.ToString();
            _httpHelpers = httpHelpers;
        }

        public async Task<CreateAccountResp> CreateEthAccount(CreateAccountModel createAccountModel)
        {
            var result = await _httpHelpers.SendPostRequestAsync(BaseUrl+"/account/create", createAccountModel);
            if (result.IsSuccessStatusCode)
            {
                using (var stream = new StreamReader(result.Content.ReadAsStream()))
                {
                    var resp = await stream.ReadToEndAsync();
                    return Decode<CreateAccountResp>(resp);
                }
            }

            return null;
        }

        public async Task<Tuple<bool,string,string>> GetLoginToken(CreateAccountModel loginModel)
        {
            try
            {
                var result = await _httpHelpers.SendPostRequestAsync(BaseUrl + "/auth/login", loginModel);
                string errString;
                string resp;
                JObject jobject;
                using(var stream = new StreamReader(result.Content.ReadAsStream()))
                {
                    resp = await stream.ReadToEndAsync();
                    jobject = JObject.Parse(resp);  
                }
                if (result.IsSuccessStatusCode)
                {
                
                
                    string accessToken = jobject.SelectToken("payload").SelectToken("access_token").ToString();
                    return new Tuple<bool, string, string>(true,accessToken,String.Empty);
                }

                if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return new Tuple<bool, string, string>(false,String.Empty,ConstantCollection.UNAUTH);
                }

                if (result.StatusCode == HttpStatusCode.BadRequest)
                {
                    string err = jobject.SelectToken("payload").SelectToken("error").ToString();
                    return new Tuple<bool, string, string>(false, String.Empty, err);
                }
                return new Tuple<bool, string, string>(false,String.Empty, String.Empty);
            }
            catch (Exception e)
            {
                return new Tuple<bool, string, string>(false, String.Empty, e.Message);
            }
            
        }

        public async Task<String> GetApiKey(GetApiKeyModel getApiKeyModel, string loginToken)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Authorization", "Bearer " + loginToken);
            var result = await _httpHelpers.SendPostRequestAsync<object>(BaseUrl + "/account/get-user", null, dict);
            if (result.IsSuccessStatusCode)
            {
                using (var stream = new StreamReader(result.Content.ReadAsStream()))
                {
                    var resp = await stream.ReadToEndAsync();
                    JObject jobject = JObject.Parse(resp);
                    string apiKeys = jobject.SelectToken("payload").SelectToken("apiKeys").ToString();
                    return apiKeys;
                }
            }

            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return ConstantCollection.UNAUTH;
            }
            return String.Empty;
        }

        public async Task<Tuple<bool,string,string>> SendEth(SendEthModel sendEthModel, string loginToken, string apiKey = null)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Authorization", "Bearer " + loginToken);
            var result =
                await _httpHelpers.SendPostRequestAsync<object>(BaseUrl + "/account/send-eth", sendEthModel, dict);
            if (result.IsSuccessStatusCode)
            {
                using (var stream = new StreamReader(result.Content.ReadAsStream()))
                {
                    var resp = await stream.ReadToEndAsync();
                    JObject jobject = JObject.Parse(resp);
                    string balance = jobject.SelectToken("payload").SelectToken("transaction").ToString();
                    return new Tuple<bool, string, string>(true,balance,String.Empty);
                }
            }
            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return new Tuple<bool, string, string>(false,String.Empty, ConstantCollection.UNAUTH);
            }
            return new Tuple<bool, string, string>(false,String.Empty, String.Empty);
        }

        public async Task<String> GetEthBalance(string loginToken, string apiKey = null)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Authorization", "Bearer " + loginToken);
            var result = await _httpHelpers.SendGetRequestAsync(BaseUrl + "/account/get-balance", null, dict);
            if (result.IsSuccessStatusCode)
            {
                using (var stream = new StreamReader(result.Content.ReadAsStream()))
                {
                    var resp = await stream.ReadToEndAsync();
                    JObject jobject = JObject.Parse(resp);
                    string balance = jobject.SelectToken("payload").SelectToken("balance").ToString();
                    return balance;
                }
            }
            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return ConstantCollection.UNAUTH;
            }
            return String.Empty;
        }

        public T Decode<T>(String source)
        {
            JObject obj = JObject.Parse(source);
            string resp = obj.SelectToken("payload").ToString();
            return JsonConvert.DeserializeObject<T>(resp);
        }

        public async Task<string> GenerateApiKey(string loginToken, CreateApiKey model)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Authorization", "Bearer " + loginToken);
            var result = await _httpHelpers.SendPostRequestAsync<CreateApiKey>(BaseUrl + "/account/generate-key",model,dict);
            if (result.IsSuccessStatusCode)
            {
                using (var stream = new StreamReader(result.Content.ReadAsStream()))
                {
                    var resp = await stream.ReadToEndAsync();
                    JObject jobject = JObject.Parse(resp);
                    string apiKeys = jobject.SelectToken("payload").SelectToken("apiKeys").ToString();
                    return apiKeys;
                }
            }
            if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return ConstantCollection.UNAUTH;
            }
            return String.Empty;
        }
    }
}