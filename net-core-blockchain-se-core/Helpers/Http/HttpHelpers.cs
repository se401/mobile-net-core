﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace net_core_blockchain_se_core.Helpers.Http
{
    public class HttpHelpers:IHttpHelpers
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly HttpClient _httpClient;

        public HttpHelpers(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            _httpClient = new HttpClient();
        }

        public async Task<HttpResponseMessage> SendGetRequestAsync(string baseUrl, string queryString,
            IDictionary<string, string> headers = null)
        {
            String requestUrl = baseUrl + "?" + queryString;
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            //var client = _clientFactory.CreateClient();
            var response = await _httpClient.SendAsync(request);
            return response;
        }

        public async Task<HttpResponseMessage> SendPostRequestAsync<T>(string baseUrl, T model, IDictionary<string, string> headers = null, String contentHeader="application/json")
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, baseUrl);
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                if(model != null)
                {
                    string dataString = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataString, Encoding.UTF8, contentHeader);
                    request.Content = stringContent;
                }
                var client = _clientFactory.CreateClient();
                var resp = await client.SendAsync(request);

                return resp;
            }
            catch (Exception e)
            {
                return null;
            }
            
        }
    }
}