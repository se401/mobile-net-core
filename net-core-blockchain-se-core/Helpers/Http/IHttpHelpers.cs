﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Helpers.Http
{
    public interface IHttpHelpers
    {
        Task<HttpResponseMessage> SendGetRequestAsync(String baseUrl, String queryString, IDictionary<string, string> header = null);

        Task<HttpResponseMessage> SendPostRequestAsync<T>(String baseUrl, T model,
            IDictionary<String, String> headers = null, String contentHeader = "application/json");
    }
}