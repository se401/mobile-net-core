﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Middleware
{
    public class CustomAuthorize
    {
        public const String LoginUser = "user";
        public const String Admin = "admin";
        public const String SupperAdmin = "supper_admin";

        private static IList<string> GetAdmin()
        {
            return new List<string> { Admin, SupperAdmin };
        }

        private static IList<string> GetUser()
        {
            return new List<string> { LoginUser };
        }

        private static IList<string> GetAllUser()
        {
            List<string> roles = new List<string>();
            roles.AddRange(GetAdmin());
            roles.AddRange(GetUser());
            return roles;
        }
        public class OnlyAdmin: AuthorizeAttribute
        {
            public OnlyAdmin()
            {
                Roles = GetAdminRoles();
            }

            private string GetAdminRoles()
            {
                return String.Join(",", GetAdmin().ToArray());
            }
        }

        public class OnlyLoginUser: AuthorizeAttribute
        {
            public OnlyLoginUser()
            {
                Roles = GetAllLoginUser();
            }

            public String GetAllLoginUser()
            {
                return String.Join(",", GetAllUser());
            }
        }
    }
}
