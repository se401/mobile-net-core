﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Middleware
{
    public interface IAuthMiddleware
    {
        string GetUserId(HttpContext httpContext);
        string GetUserRole(HttpContext httpContext);
    }
}
