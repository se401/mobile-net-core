﻿using Microsoft.AspNetCore.Http;
using net_core_blockchain_se_core.Security.Jwt;
using System;

namespace net_core_blockchain_se_core.Middleware
{
    public class AuthMiddleware:IAuthMiddleware
    {
        private readonly IJwtMaster _jwtMaster;
        public AuthMiddleware(IJwtMaster jwtMaster)
        {
            _jwtMaster = jwtMaster;
        }

        public string GetUserId(HttpContext htppContext)
        {
            string authHeader = htppContext.Request.Headers["Authorization"].ToString();

            string[] parts = authHeader.Split(" ");

            string token = String.Empty;

            if (parts.Length > 1)
                token = parts[1];
            var payload = _jwtMaster.VerifyToken(token);

            if (payload == null)
                return String.Empty;

            if(payload.Count > 0 && payload.ContainsKey("Id"))
            {
                return payload["Id"].ToString();
            }
            return String.Empty;
        }

        public string GetUserRole(HttpContext htppContext)
        {
            string authHeader = htppContext.Request.Headers["Authorization"].ToString();

            string[] parts = authHeader.Split(" ");

            string token = String.Empty;

            if (parts.Length > 1)
                token = parts[1];

            var payload = _jwtMaster.VerifyToken(token);

            if (payload == null)
                return String.Empty;

            if (payload.Count > 0 && payload.ContainsKey("role"))
            {
                return payload["role"].ToString();
            }
            return String.Empty;
        }
    }
}
