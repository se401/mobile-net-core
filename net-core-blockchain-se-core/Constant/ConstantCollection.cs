﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Constant
{
    public class ConstantCollection
    {
        #region DB
        public const string DB_User = "user";
        public const string DB_Posts = "post";
        #endregion

        #region Collection
        public const string User_Collection = "account";
        public const string Category_Collection = "category";
        public const string Resource_Collection = "resource";
        public const string Level_Collection = "level";
        public const string Post_Collection = "post";
        public const string Commet_Collection = "comment";
        public const string Transaction_Collection = "transaction";
        #endregion

        #region Redis Hash Key
        public const string EMAIL_COLLECTION = "REGISTERED_EMAIL";
        public const string VIEW_COUNT_COLLECTION = "VIEW_COUNT";
        #endregion

        #region Round
        public const int Round = 1000;
        #endregion

        #region  Path
        public const String categoryPath = "/mobile/category";
        public const String avatartPath = "/mobile/user";
        public const String postPath = "/mobile/post";
        #endregion

        #region Ethereum roles
        public const string TRANSACTION = "transaction";
        public const string BALANCE = "balance";
        public const string UNAUTH = "Unauthorized";
        #endregion

        public enum TransferType
        {
            SEND = 1,
            RECEIVE = 2,
            CASHBACK = 3
        }
        public enum TransactionType
        {
            RECEIVED_PAYMENT = 1,
            PAY_PAYMENT = 2,
            
        }

        
    }
}
