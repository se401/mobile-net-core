﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Constant
{
    public class Enums
    {
        public enum SORT
        {
            ASC = 1,
            DSC = -1,
        }
        public enum PostState
        {
            PENDING_APPROVE = 1,
            COMPLETE = 2,
            UNAVAILABLE = 3
        }
    }
}
