using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.AspNetCore.Http;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Domain.Category.Commands;
using net_core_blockchain_se_core.Domain.Category.Resps;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Infra.Data.DTO.Category;
using net_core_blockchain_se_core.Infra.Repository.Category;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Utility.Cloud;
using net_core_blockchain_se_core.Utility.Img;
using net_core_blockchain_se_core.Utility.Mapper;

namespace net_core_blockchain_se_core.Domain.Category.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public Resp<CategoryResp> CreateCategory(CommandCreateCategory commandCreateCategory, IFormFile file = null)
        {
            try
            {
                CategoryDTO categoryDTO = new CategoryDTO
                {
                    Name = commandCreateCategory.Name,
                    Description = commandCreateCategory.Description
                };

                if (!String.IsNullOrEmpty(commandCreateCategory.ParentId))
                {
                    //get parent
                    var parentCateogry = _categoryRepository.GetById(commandCreateCategory.ParentId);
                    if (parentCateogry != null)
                        categoryDTO.Parent = parentCateogry;
                }

                //handle file

                if (file != null)
                {
                    Image img = Image.FromStream(file.OpenReadStream());
                    string base64Str = ImgUtil.ImageToBase64String(img, 200, 200);
                    string imgUrl = CloudinaryUtil.getInstance.UploadImageToCloudinary(base64Str, ConstantCollection.categoryPath);
                    if (!String.IsNullOrEmpty(imgUrl))
                    {
                        categoryDTO.ThumnailUrl = imgUrl;
                    }
                }

                _categoryRepository.Insert(categoryDTO);
                var mapp = Mapping.Mapper.Map<CategoryResp>(categoryDTO);

                return new Resp<CategoryResp>(System.Net.HttpStatusCode.OK, String.Empty, mapp);
            }
            catch (System.Exception e)
            {
                return new Resp<CategoryResp>(System.Net.HttpStatusCode.BadRequest, e.Message, null);
            }
        }

        public Resp<PagedList<CategoryResp>> GetActiveCategories(Filter filter = null)
        {
            try
            {
                var categories = _categoryRepository.GetActiveCategories();
                var map = Mapping.Mapper.Map<IEnumerable<CategoryResp>>(categories);
                map = RespExtensions<CategoryResp>.ReflactorObject(map, filter);
                if (filter.PageNumber == 0)
                    return new Resp<PagedList<CategoryResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<CategoryResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<CategoryResp>(filter, map)
                    );
                return new Resp<PagedList<CategoryResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<CategoryResp>.ToPagedList(map, filter.PageNumber, filter.PageSize), filter,
                    Paging.ToPaging<CategoryResp>(filter,
                                                  map)
                );
            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<CategoryResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    String.Empty,
                    null, filter
                );
            }
        }

        public Resp<PagedList<CategoryResp>> GetAllCategories(Filter filter = null)
        {
            try
            {
                var categories = _categoryRepository.GetAll();
                var map = Mapping.Mapper.Map<IEnumerable<CategoryResp>>(categories);
                map = RespExtensions<CategoryResp>.ReflactorObject(map, filter);
                if (filter.PageNumber == 0)
                    return new Resp<PagedList<CategoryResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<CategoryResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<CategoryResp>(filter,map)
                    );
                return new Resp<PagedList<CategoryResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<CategoryResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<CategoryResp>(filter,map)
                );
            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<CategoryResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    String.Empty,
                    null,
                    filter
                );
            }
        }

        public Resp<CategoryResp> UpdateCategory(string categoryId, CommandUpdateCategory cmdUpdateCategory, IFormFile file = null)
        {
            try
            {
                var categoryFromDb = _categoryRepository.GetById(categoryId);
                if (categoryFromDb == null)
                    return new Resp<CategoryResp>(System.Net.HttpStatusCode.BadRequest, String.Format("Cannot find the object corresponding to the id {0}", categoryId), null);
                if (categoryFromDb.Parent != null)
                {
                    if (String.IsNullOrEmpty(cmdUpdateCategory.ParentId))
                    {
                        categoryFromDb.Parent = null;
                    }
                    else if (categoryFromDb.Parent.ToString() != cmdUpdateCategory.ParentId)
                    {
                        var newParent = _categoryRepository.GetById(cmdUpdateCategory.ParentId);
                        categoryFromDb.Parent = newParent;
                    }

                }
                else
                {
                    if (!String.IsNullOrEmpty(cmdUpdateCategory.ParentId))
                    {
                        var newParent = _categoryRepository.GetById(cmdUpdateCategory.ParentId);
                        categoryFromDb.Parent = newParent;
                    }
                }

                if (file != null)
                {
                    Image img = Image.FromStream(file.OpenReadStream());
                    string base64Str = ImgUtil.ImageToBase64String(img, 200, 200);
                    string imgUrl = CloudinaryUtil.getInstance.UploadImageToCloudinary(base64Str, ConstantCollection.categoryPath);
                    if (!String.IsNullOrEmpty(imgUrl))
                    {
                        categoryFromDb.ThumnailUrl = imgUrl;
                    }
                }

                categoryFromDb.IsActive = cmdUpdateCategory.IsActive;
                categoryFromDb.Name = cmdUpdateCategory.Name;
                categoryFromDb.Description = cmdUpdateCategory.Description;

                _categoryRepository.UpdateOne(categoryId, categoryFromDb);
                var map = Mapping.Mapper.Map<CategoryResp>(categoryFromDb);
                return new Resp<CategoryResp>(System.Net.HttpStatusCode.OK, String.Empty, map);
            }
            catch (System.Exception e)
            {
                return new Resp<CategoryResp>(System.Net.HttpStatusCode.BadRequest, e.Message, null);
            }
        }
    }
}