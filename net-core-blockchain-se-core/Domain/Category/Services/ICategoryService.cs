using Microsoft.AspNetCore.Http;
using net_core_blockchain_se_core.Domain.Category.Commands;
using net_core_blockchain_se_core.Domain.Category.Resps;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Category.Services
{
    public interface ICategoryService
    {
        Resp<CategoryResp> CreateCategory(CommandCreateCategory commandCreateCategory, IFormFile file = null);
        Resp<CategoryResp> UpdateCategory(string categoryId, CommandUpdateCategory cmdUpdateCategory, IFormFile file = null);

        Resp<PagedList<CategoryResp>> GetActiveCategories(Filter filter = null);

        Resp<PagedList<CategoryResp>> GetAllCategories(Filter filter = null);
    }
}