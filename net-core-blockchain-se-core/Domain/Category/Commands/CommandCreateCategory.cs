﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Domain.Category.Commands
{
    public class CommandCreateCategory
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string ParentId { get; set; }
    }
}
