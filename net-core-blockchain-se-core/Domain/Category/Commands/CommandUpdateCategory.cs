namespace net_core_blockchain_se_core.Domain.Category.Commands
{
    public class CommandUpdateCategory
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ParentId { get; set; }
        public bool IsActive { get; set; } = true;
    }
}