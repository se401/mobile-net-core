using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Category.Resps
{
    public class CategoryResp:BaseResp
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumnailUrl { get; set; }
    }
}