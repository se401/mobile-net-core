namespace net_core_blockchain_se_core.Domain.User.Commands
{
    public class CommandUpdateUserInfo
    {
        public string Address { get; set; }
        public string DisplayName { get; set; }
    }
}