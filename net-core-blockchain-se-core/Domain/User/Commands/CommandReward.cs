﻿using System;

namespace net_core_blockchain_se_core.Domain.User.Commands
{
    public class CommandReward
    {
        public string Token { get; set; }
        public Decimal Amount { get; set; }
    }
}