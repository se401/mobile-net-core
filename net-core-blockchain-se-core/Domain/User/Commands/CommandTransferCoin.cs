﻿using System;
using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.User.Commands
{
    public class CommandTransferCoin
    {
        [Required]
        public string Address { get; set; }
        [Required]
        public Decimal Amount { get; set; }
    }
}