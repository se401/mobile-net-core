using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.User.Commands
{
    public class CommandChangePassword
    {
        [Required]
        [StringLength(maximumLength:64,ErrorMessage="Passowrd must be between 8 and 64 characters",MinimumLength=8)]
        public string Password { get; set; }

        [Required]
        [Compare("Password",ErrorMessage="Confirm password does not match")]
        public string ConfirmPassword { get; set; }
    }
}