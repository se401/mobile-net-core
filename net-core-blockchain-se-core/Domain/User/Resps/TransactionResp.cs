﻿using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.User.Resps
{
    public class TransactionResp:BaseResp
    {
        public string UserId { get; set; }
        public string FromWalletAddress { get; set; }
        public decimal TransactionAmount { get; set; }
        public ConstantCollection.TransactionType TransactionType { get; set; }
        public string ToWalletAddress { get; set; }
        public string Description { get; set; }
    }
}