using System;
using System.Collections.Generic;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.User.Resps
{
    public class UserResp:BaseResp
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string DisplayName { get; set; }
        public DateTime LastAccessedDate { get; set; }
        public string AvatarUrl { get; set; }
        public IList<string> Bookmarks { get; set; }
        public IList<string> LikedPosts { get; set; }
        public String WalletAddress { get; set; }
    }
}