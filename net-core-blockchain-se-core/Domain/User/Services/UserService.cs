﻿using Microsoft.AspNetCore.Http;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Domain.User.Commands;
using net_core_blockchain_se_core.Domain.User.Resps;
using net_core_blockchain_se_core.Helpers.Redis;
using net_core_blockchain_se_core.Infra.Repository.User;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Security.Brypt;
using net_core_blockchain_se_core.Utility.Cloud;
using net_core_blockchain_se_core.Utility.Img;
using net_core_blockchain_se_core.Utility.Mapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CloudinaryDotNet;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Infra.Repository.Posts;
using net_core_blockchain_se_core.Helpers.Ethereum;
using net_core_blockchain_se_core.Helpers.Ethereum.EthModels;
using net_core_blockchain_se_core.Infra.Data.DTO.Transaction;
using net_core_blockchain_se_core.Infra.Data.DTO.User;
using net_core_blockchain_se_core.Infra.Repository.Transaction;
using net_core_blockchain_se_core.Utility.Others;
using Newtonsoft.Json;

namespace net_core_blockchain_se_core.Domain.User.Services
{
    public class UserService : IUserService
    {
        private readonly IAccountRepository _accountRepository;

        private readonly IPostRepository _postRepository;
        private readonly IBcryptMaster _bcryptMaster;
        private readonly IEthHelpers _ethHelpers;
        private readonly IJedisMaster _jedisMaster;
        private readonly ITransactionRepository _transactionRepository;
        private static string AdminWalletAddr = "0x5738Ae2d4E07A4025e04996392d1a797B4A88401";
        public UserService(IAccountRepository accountRepository, IBcryptMaster bcryptMaster,
            IPostRepository postRepository,
            IEthHelpers ethHelpers,
            IJedisMaster jedisMaster,
            ITransactionRepository transactionRepository)
        {
            _accountRepository = accountRepository;
            _bcryptMaster = bcryptMaster;
            _postRepository = postRepository;
            _ethHelpers = ethHelpers;
            _jedisMaster = jedisMaster;
            _transactionRepository = transactionRepository;
        }

        public Resp<bool> ChangeAvatar(string userId, IFormFile file)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                if (userInDb == null)
                    return new Resp<bool>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the object corresponding to the id {0}", userId),
                        false
                    );
                if (file != null)
                {
                    Image img = Image.FromStream(file.OpenReadStream());
                    string base64Str = ImgUtil.ImageToBase64String(img, 200, 200);
                    string imgUrl = CloudinaryUtil.getInstance.UploadImageToCloudinary(base64Str, ConstantCollection.avatartPath);
                    if (!String.IsNullOrEmpty(imgUrl))
                    {
                        userInDb.AvatarUrl = imgUrl;
                    }
                }

                _accountRepository.UpdateOne(userId, userInDb);
                return new Resp<bool>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    true
                );

            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    System.Net.HttpStatusCode.BadRequest,
                    e.Message,
                    false
                );
            }
        }

        public Resp<UserResp> ChangePassword(string userId, CommandChangePassword commandChange)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                if (userInDb == null)
                    return new Resp<UserResp>(System.Net.HttpStatusCode.BadRequest, String.Format("Cannot find the user corresponding to the id {0}", userId), null);
                if (_bcryptMaster.VerifyPassword(commandChange.Password, userInDb.PasswordHash))
                    return new Resp<UserResp>(System.Net.HttpStatusCode.BadRequest, "Your provided password is being used on your account", null);

                string passwordHash = _bcryptMaster.GenerateHash(commandChange.Password);

                userInDb.PasswordHash = passwordHash;

                _accountRepository.UpdateOne(userInDb._id.ToString(), userInDb);

                var map = Mapping.Mapper.Map<UserResp>(userInDb);
                return new Resp<UserResp>(System.Net.HttpStatusCode.OK,
                String.Empty,
                map);
            }
            catch (System.Exception e)
            {

                return new Resp<UserResp>(
                    System.Net.HttpStatusCode.BadRequest,
                    e.Message,
                    null
                );
            }
        }

        public Resp<UserResp> GetUserInfo(string userId)
        {
            try
            {
                var user = _accountRepository.GetById(userId);
                
                if (user == null)
                    return new Resp<UserResp>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the user corresponding to the id {0}", userId),
                        null
                    );
                var map = Mapping.Mapper.Map<UserResp>(user);
                return new Resp<UserResp>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    map
                );
            }
            catch (Exception e)
            {
                return new Resp<UserResp>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null);
            }
            
        }

        public Resp<bool> RemoveBookmark(string userId, string postId)
        {
            var userInDb = _accountRepository.GetById(userId);
            userInDb.Bookmarks.Remove(postId);

            _accountRepository.UpdateOne(userId, userInDb);

            return new Resp<bool>(
                HttpStatusCode.OK,
                String.Empty,
                true);
        }

        public async Task<Resp<bool>> CreateApiKey(string userId, CreateApiKey model)
        {
            try
            {
                var user = _accountRepository.GetById(userId);

                if (user == null)
                    return new Resp<bool>(HttpStatusCode.BadRequest,
                        $"Cannot find the user corresponding to the id {userId}",
                        false);
                if (model.roles.Length == 0)
                    return new Resp<bool>(HttpStatusCode.BadRequest,
                        "Role length must not be empty",
                        false);

                string key = "jwt_" + userId;
                //get login token
                string val = _jedisMaster.Pull(0, key);
                string token;
                if (String.IsNullOrEmpty(val))
                {
                    //login again
                    var resp = await _ethHelpers.GetLoginToken(new Helpers.Ethereum.EthModels.CreateAccountModel(user.Email,user.PasswordHash));

                    if (resp.Item1)
                    {
                        token = resp.Item2;
                        _jedisMaster.Push(0, key, token, TimeSpan.FromHours(12));
                    }
                    else
                    {
                        return new Resp<bool>(
                            HttpStatusCode.BadRequest,
                            "ETH: "+ resp.Item3,
                            false);
                    }
                    
                }
                else
                {
                    token = val;
                }

                string apiKeys = await _ethHelpers.GenerateApiKey(token, model);

                var apis = JsonConvert.DeserializeObject<List<ApiKey>>(apiKeys);

                user.ApiKeys = apis;
                _accountRepository.UpdateOne(userId, user);
                return new Resp<bool>(HttpStatusCode.OK,
                    String.Empty,
                    true);
            }
            catch (Exception e)
            {

                return new Resp<bool>(HttpStatusCode.BadGateway,
                    e.Message,
                    false);
            }
        }

        public async Task<Resp<bool>> DropApiKey(string userId, string key)
        {
            throw new NotImplementedException();
        }

        public Resp<PagedList<UserResp>> GetUsers(Filter filter = null)
        {
            var users = _accountRepository.GetAll();
            var map = Mapping.Mapper.Map<IEnumerable<UserResp>>(users);

            if (filter.PageNumber == 0)
                return new Resp<PagedList<UserResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<UserResp>.ToPagedList(map, 1, map.Count()),
                    filter,
                    Paging.ToPaging<UserResp>(filter, map)
                );
            return new Resp<PagedList<UserResp>>(
                System.Net.HttpStatusCode.OK,
                String.Empty,
                PagedList<UserResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                filter,
                Paging.ToPaging<UserResp>(filter, map)
            );
        }

        public Resp<UserResp> SaveBookmark(string userId, string postId)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                if (userInDb == null)
                    return new Resp<UserResp>(System.Net.HttpStatusCode.BadRequest, String.Format("Cannot find the user corresponding to the id {0}", userId), null);
                //check post id

                var postInDb = _postRepository.GetById(postId);

                if (postInDb == null)
                {
                    return new Resp<UserResp>(
                        HttpStatusCode.BadRequest,
                        $"Cannot find the post corresponding to the id {postId}",
                        null);

                }
                
                if (!userInDb.Bookmarks.Any(x => x == postId))
                {
                    userInDb.Bookmarks.Add(postId);
                    _accountRepository.UpdateOne(userId, userInDb);
                }

                var map = Mapping.Mapper.Map<UserResp>(userInDb);

                return new Resp<UserResp>(System.Net.HttpStatusCode.OK,
                String.Empty, map
                );
            }
            catch (System.Exception ex)
            {

                return new Resp<UserResp>(
                    System.Net.HttpStatusCode.BadRequest,
                    ex.Message,
                    null
                );
            }
        }

        public Resp<UserResp> UpdateUserInfo(string userId, CommandUpdateUserInfo command)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);

                if (userInDb == null)
                    return new Resp<UserResp>(System.Net.HttpStatusCode.BadRequest,
                    String.Format("Cannot find the user corresponding to the id {0}", userId), null);
                if (!String.IsNullOrEmpty(command.DisplayName))
                    userInDb.DisplayName = command.DisplayName;
                if (!String.IsNullOrEmpty(command.Address))
                    userInDb.Address = command.Address;

                _accountRepository.UpdateOne(userId, userInDb);

                var map = Mapping.Mapper.Map<UserResp>(userInDb);
                return new Resp<UserResp>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    map
                );
            }
            catch (System.Exception e)
            {

                return new Resp<UserResp>(
                    System.Net.HttpStatusCode.BadRequest,
                    e.Message,
                    null
                );
            }
        }

        public async Task<Resp<Decimal>> GetBalance(string userId)
        {
            try
            {
                var user = await _accountRepository.GetByIdAsync(userId);
                string key = "jwt_" + userId;
                //get login token
                string val = _jedisMaster.Pull(0, key);
                string token;
                if (String.IsNullOrEmpty(val))
                {
                    //login again
                    var resp = await _ethHelpers.GetLoginToken(new Helpers.Ethereum.EthModels.CreateAccountModel(user.Email,user.PasswordHash));

                    if (resp.Item1)
                    {
                        token = resp.Item2;
                        _jedisMaster.Push(0, key, token, TimeSpan.FromHours(12));
                    }
                    else
                    {
                        return new Resp<Decimal>(
                            HttpStatusCode.BadRequest,
                            "ETH: "+ resp.Item3,
                            0);
                    }
                }
                else
                {
                    token = val;
                }

                var balance = await _ethHelpers.GetEthBalance(token);

                Decimal balanceDecimal = Decimal.TryParse(balance, out balanceDecimal) ? balanceDecimal : 0;

                return new Resp<decimal>(
                    HttpStatusCode.OK,
                    String.Empty,
                    balanceDecimal);

            }
            catch (Exception e)
            {
                return new Resp<decimal>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    0);
            }
        }

        public async Task<Resp<bool>> Transfer(string userId, CommandTransferCoin cmd)
        {
            try
            {
                var user = await _accountRepository.GetByIdAsync(userId);

                var destinationAddrAcc = await _accountRepository.GetByWalletAddress(cmd.Address);

                if (destinationAddrAcc == null)
                {
                    return new Resp<bool>(HttpStatusCode.BadRequest,
                        $"Cannot find the user corresponding to the address {cmd.Address}",
                        false);
                }


                
                string key = "jwt_" + userId;
                //get login token
                string val = _jedisMaster.Pull(0, key);
                string token;
                if (String.IsNullOrEmpty(val))
                {
                    //login again
                    var resp = await _ethHelpers.GetLoginToken(new Helpers.Ethereum.EthModels.CreateAccountModel(user.Email,user.PasswordHash));

                    if (resp.Item1)
                    {
                        token = resp.Item2;
                        _jedisMaster.Push(0, key, token, TimeSpan.FromHours(12));
                    }
                    else
                    {
                        return new Resp<bool>(
                            HttpStatusCode.BadRequest,
                            "ETH: "+ resp.Item3,
                            false);
                    }
                }
                else
                {
                    token = val;
                }
                
                var balance = await _ethHelpers.GetEthBalance(token);

                Decimal balanceDecimal = Decimal.TryParse(balance, out balanceDecimal) ? balanceDecimal : 0;

                if (balanceDecimal<cmd.Amount)
                {
                    return new Resp<bool>(HttpStatusCode.BadRequest,
                        $"Balance is insufficient to make the transaction",
                        false);
                }
                
                var model = new SendEthModel()
                {
                    fromAddr = user.WalletAddress,
                    toAddr = cmd.Address,
                    amount = cmd.Amount.ToString()
                };

                var result = await _ethHelpers.SendEth(model, token);
                if (!result.Item1)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"ETH: {result.Item3}",
                        false);
                }

                List<TransactionDTO> trans = new List<TransactionDTO>();
                
                TransactionDTO srcTrans = new TransactionDTO()
                {
                    UserId = userId,
                    Description = $"SEND ETH: {cmd.Amount} coin to address {cmd.Address}",
                    FromWalletAddress = user.WalletAddress,
                    ToWalletAddress = cmd.Address
                };
                
                TransactionDTO dstTrans = new TransactionDTO()
                {
                    UserId = destinationAddrAcc._id.ToString(),
                    Description = $"RECEIVED ETH: {cmd.Amount} coin from address {user.WalletAddress}",
                    FromWalletAddress = cmd.Address,
                    //ToWalletAddress = user.Address
                };
                
                trans.Add(srcTrans);
                trans.Add(dstTrans);

                _transactionRepository.InsertManyAsync(trans);
                
                return new Resp<bool>(HttpStatusCode.OK,
                    result.Item2,
                    true);
            }
            catch (Exception e)
            {
                return new Resp<bool>(HttpStatusCode.BadGateway,
                    e.Message,
                    false);
            }
        }

        public Resp<PagedList<TransactionResp>> GetTransactions(string userId, Filter filter = null)
        {
            var trans = _transactionRepository.GetTransactionByUser(userId);
            var resp = RespExtensions<TransactionDTO>.ReflactorObject(trans, filter);
            var maps = Mapping.Mapper.Map<IEnumerable<TransactionResp>>(resp);
            
            if (filter.PageSize == 0)
            {
                return new Resp<PagedList<TransactionResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<TransactionResp>.ToPagedList(maps, 1, 1),
                    filter,
                    Paging.ToPaging<TransactionResp>(filter, maps)
                );
            }
            return new Resp<PagedList<TransactionResp>>(
                System.Net.HttpStatusCode.OK,
                String.Empty,
                PagedList<TransactionResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                filter,
                Paging.ToPaging<TransactionResp>(filter, maps)
            );
        }

        public Resp<string> GetTokenReward(string userId)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                if (userInDb == null)
                {
                    return new Resp<string>(HttpStatusCode.BadRequest,
                        $"Can not find the user corresponding to the id {userId}",
                        String.Empty);
                }

                string key = "reward_" + userId;
                string cached = _jedisMaster.Pull(0, key);
                if (!String.IsNullOrEmpty(cached))
                {
                    return new Resp<string>(
                        HttpStatusCode.BadRequest,
                        $"You already have a token",
                        String.Empty);
                }

                string randomString = TextUtil.GenerateRandomText(16);
                String token = _bcryptMaster.GenerateHash(randomString);
                _jedisMaster.Push(0,key,token,TimeSpan.FromMinutes(15));
                return new Resp<string>(
                    HttpStatusCode.OK,
                    String.Empty,
                    token);
            }
            catch (Exception e)
            {
                return new Resp<string>(
                    HttpStatusCode.OK,
                    e.Message,
                    String.Empty);
            }
        }

        public Resp<bool> Reward(string userId, CommandReward cmd)
        {
            try
            {
                var user = _accountRepository.GetById(userId);
                if (user == null)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"Can not find the user corresponding to the id {userId}",
                        false);
                }

                string key = "reward_" + userId;
                string cached = _jedisMaster.Pull(0, key);
                
                if (String.IsNullOrEmpty(cached))
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"Token is expired",
                        false);
                }

                if (cached != cmd.Token)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadGateway,
                        $"Invalid Token",
                        false);
                }

                _jedisMaster.DeleteKey(0, key);

                var adminUser = _accountRepository.GetByWalletAddress(AdminWalletAddr).Result;

                var result = Transfer(adminUser._id.ToString(), new CommandTransferCoin()
                {
                    Address = user.WalletAddress,
                    Amount = cmd.Amount
                }).Result;
                if (result.Payload)
                {
                    return new Resp<bool>(
                        HttpStatusCode.OK,
                        String.Empty,
                        true);
                }

                return new Resp<bool>(
                    HttpStatusCode.BadGateway,
                    result.Message,
                    false);

            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    false);

            }
        }
    }
}
