﻿using Microsoft.AspNetCore.Http;
using net_core_blockchain_se_core.Domain.User.Commands;
using net_core_blockchain_se_core.Domain.User.Resps;
using net_core_blockchain_se_core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using net_core_blockchain_se_core.Helpers.Ethereum.EthModels;

namespace net_core_blockchain_se_core.Domain.User.Services
{
    public interface IUserService
    {
        Resp<UserResp> SaveBookmark(string userId, string postId);
        Resp<UserResp> UpdateUserInfo(string userId, CommandUpdateUserInfo command);
        Resp<UserResp> ChangePassword(string userId, CommandChangePassword commandChange);
        Resp<bool> ChangeAvatar(string userId, IFormFile file);
        Resp<PagedList<UserResp>> GetUsers(Filter filter = null);
        Resp<UserResp> GetUserInfo(string userId);
        Resp<bool> RemoveBookmark(string userId, string postId);
        Task<Resp<bool>> CreateApiKey(string userId, CreateApiKey model);
        Task<Resp<bool>> DropApiKey(string userId, string key);
        Task<Resp<Decimal>> GetBalance(string userId);
        Task<Resp<bool>> Transfer(string userId, CommandTransferCoin cmd);
        Resp<PagedList<TransactionResp>> GetTransactions(string userId, Filter filter = null);
        Resp<String> GetTokenReward(string userId);
        Resp<bool> Reward(string userId, CommandReward cmd);
    }
}
