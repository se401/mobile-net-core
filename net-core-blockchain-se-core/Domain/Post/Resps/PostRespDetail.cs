﻿using System;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Domain.Authentication.Resps;
using net_core_blockchain_se_core.Domain.Category.Resps;
using net_core_blockchain_se_core.Domain.Level.Resps;
using net_core_blockchain_se_core.Domain.Resource.Resps;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Post.Resps
{
    public class PostRespDetail:BaseResp
    {
        public String Title { get; set; }
        public String BackgroudUrl { get; set; }
        public String Description { get; set; } 
        public int ViewCount { get; set; }
        public AccountResp Author { get; set; }
        public CategoryResp Category { get; set; }
        public LevelResp Level { get; set; }
        public ResourceResp Resource { get; set; }
        public int LikeCount { get; set; }
        public Decimal Price { get; set; }
        public Enums.PostState Status { get; set; }
    }
}