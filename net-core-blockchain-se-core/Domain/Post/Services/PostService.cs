using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DnsClient.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Domain.Post.Commands;
using net_core_blockchain_se_core.Domain.Post.Resps;
using net_core_blockchain_se_core.Domain.User.Commands;
using net_core_blockchain_se_core.Domain.User.Services;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Helpers.IO;
using net_core_blockchain_se_core.Helpers.RabbitMQ;
using net_core_blockchain_se_core.Helpers.Redis;
using net_core_blockchain_se_core.Infra.Data.DTO.Post;
using net_core_blockchain_se_core.Infra.Data.DTO.Transaction;
using net_core_blockchain_se_core.Infra.Repository.Category;
using net_core_blockchain_se_core.Infra.Repository.Level;
using net_core_blockchain_se_core.Infra.Repository.Posts;
using net_core_blockchain_se_core.Infra.Repository.Resource;
using net_core_blockchain_se_core.Infra.Repository.Transaction;
using net_core_blockchain_se_core.Infra.Repository.User;
using net_core_blockchain_se_core.Middleware;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Utility.Cloud;
using net_core_blockchain_se_core.Utility.Img;
using net_core_blockchain_se_core.Utility.Mapper;
using Newtonsoft.Json;
using ILogger = DnsClient.Internal.ILogger;

namespace net_core_blockchain_se_core.Domain.Post.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IResourceRepository _resourceRepository;
        private readonly ILevelRepository _levelRepository;
        private readonly IJedisMaster _jedisMaster;
        private readonly IAQMPMaster _rabbitMQ;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IUserService _userService;

        private readonly IConfiguration _configuration;

        private string QUEUE_UPDATE_POST;

        private readonly ILogger<PostService> _logger;
        public PostService(IPostRepository postRepository,
                            ICategoryRepository categoryRepository,
                            IResourceRepository resourceRepository,
                            IAccountRepository accountRepository,
                            ILevelRepository levelRepository,
                            IJedisMaster jedisMaster,
                            IAQMPMaster rabbit,
                            IConfiguration configuration,
                            ITransactionRepository transactionRepository,
                            IUserService userService,
                            ILogger<PostService> logger
        )
        {
            _accountRepository = accountRepository;
            _postRepository = postRepository;
            _categoryRepository = categoryRepository;
            _levelRepository = levelRepository;
            _resourceRepository = resourceRepository;
            _jedisMaster = jedisMaster;
            _rabbitMQ = rabbit;
            _transactionRepository = transactionRepository;
            _configuration = configuration;
            _userService = userService;
            _logger = logger;

            QUEUE_UPDATE_POST = _configuration.GetSection("AQMP:QUEUE_UPDATE_POST").Value.ToString();
        }

        public Resp<bool> ApprovePost(string postId, Decimal price)
        {
            try
            {
                var postInDb = _postRepository.GetById(postId);
                if (postInDb == null)
                    return new Resp<bool>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the post corresponding to the id {0}", postId),
                        false
                    );

                postInDb.Status = Enums.PostState.COMPLETE;
                if (price > 0)
                {
                    postInDb.Price = price;
                }
                _postRepository.UpdateOne(postId,postInDb);

                return new Resp<bool>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    true
                );
            }
            catch (System.Exception ex)
            {

                return new Resp<bool>(
                    System.Net.HttpStatusCode.BadGateway,
                    ex.Message,
                    false
                );
            }
        }

        public Resp<PostResp> CreatePost(string authorId, CommandCreatePost cmd)
        {
            _logger.LogDebug(JsonConvert.SerializeObject(cmd));
            try
            {
                var accountDTO = _accountRepository.GetById(authorId);
                if (accountDTO == null)
                    return new Resp<PostResp>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the user corresponding to the id {0}", authorId),
                        null
                    );

                var categoryDTO = _categoryRepository.GetById(cmd.CategoryId);
                if (categoryDTO == null)
                    return new Resp<PostResp>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the category corresponding to the id {0}", cmd.CategoryId),
                        null
                    );

                var levelDto = _levelRepository.GetById(cmd.LevelId);
                if (levelDto == null)
                    return new Resp<PostResp>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the level corresponding to the id {0}", cmd.LevelId),
                        null
                    );

                var resourceDto = _resourceRepository.GetById(cmd.ResourceId);
                if (resourceDto == null)
                    return new Resp<PostResp>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the resource corresponding to the id {0}", cmd.ResourceId),
                        null
                    );

                PostDTO post = new PostDTO
                {
                    Author = accountDTO,
                    Level = levelDto,
                    Category = categoryDTO,
                    Resource = resourceDto,
                    Title = cmd.Title,
                    Description = cmd.Description,
                    Price = cmd.Price
                };

                if (cmd.Background != null)
                {
                    
                    string imgUrl = CloudinaryUtil.getInstance.UploadFileToCloudinary(cmd.Background.OpenReadStream(), ConstantCollection.categoryPath);
                    if (imgUrl.Substring(0,2) == "99")
                    {
                        _logger.LogDebug(imgUrl);
                    }else if (!String.IsNullOrEmpty(imgUrl))
                    {
                        post.BackgroudUrl = imgUrl;
                    }
                    
                }

                // string url = CloudinaryUtil.getInstance.UploadFileToCloudinary(cmd.Source.OpenReadStream(), ConstantCollection.DB_Posts);
                post.Url = cmd.Source;

                _postRepository.Insert(post);

                var map = Mapping.Mapper.Map<PostResp>(post);

                return new Resp<PostResp>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    map
                );

            }
            catch (System.Exception e)
            {

                return new Resp<PostResp>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null
                );
            }
        }

        public Resp<bool> EditPost(string userId, string postId, CommandEditPost cmd)
        {
            try
            {

                var user = _accountRepository.GetById(userId);

                if (user == null)
                    return new Resp<bool>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the user corresponding the id {0}", userId),
                        false
                    );

                var postInDb = _postRepository.GetById(postId);

                if (postInDb == null)
                    return new Resp<bool>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the post corresponding to the id {0}", postId),
                        false
                    );
                if (!user.Role.Equals(CustomAuthorize.Admin))
                {
                    if (!user._id.Equals(postInDb.Author._id))
                        return new Resp<bool>(
                            System.Net.HttpStatusCode.BadRequest,
                            "The post is belong to the other owner",
                            false
                        );
                }
                

                if (postInDb.Status == Enums.PostState.COMPLETE)
                {
                    if (user.Role == CustomAuthorize.LoginUser)
                        return new Resp<bool>(
                            System.Net.HttpStatusCode.BadRequest,
                            "You cannot edit the post in the current state",
                            false
                        );
                }

                EditHistory history = new EditHistory();
                history.CurrentData = postInDb;
                history.Editor = user;

                if (postInDb.EditHistories == null)
                {
                    postInDb.EditHistories = new List<EditHistory>();
                }

                if (!String.IsNullOrEmpty(cmd.Title))
                    postInDb.Title = cmd.Title;
                if (!String.IsNullOrEmpty(cmd.CategoryId) && !postInDb.Category._id.ToString().Equals(cmd.CategoryId))
                {
                    var category = _categoryRepository.GetById(cmd.CategoryId);
                    if (category != null)
                    {
                        postInDb.Category = category;
                    }
                }

                if (!String.IsNullOrEmpty(cmd.LevelId) && !postInDb.Level._id.ToString().Equals(cmd.LevelId))
                {
                    var level = _levelRepository.GetById(cmd.LevelId);
                    if (level != null)
                    {
                        postInDb.Level = level;
                    }
                }

                if (!String.IsNullOrEmpty(cmd.ResourceId) && !postInDb.Resource._id.ToString().Equals(cmd.ResourceId))
                {
                    var resource = _resourceRepository.GetById(cmd.ResourceId);
                    if (resource != null)
                    {
                        postInDb.Resource = resource;
                    }
                }

                if (cmd.Background != null)
                {
                    string url = CloudinaryUtil.getInstance.UploadFileToCloudinary(cmd.Background.OpenReadStream(), ConstantCollection.postPath);
                    postInDb.BackgroudUrl = url;
                }

                if (!String.IsNullOrEmpty(cmd.Source))
                {
                    // string url = CloudinaryUtil.getInstance.UploadFileToCloudinary(cmd.Source.OpenReadStream(), ConstantCollection.postPath);
                    postInDb.Url = cmd.Source;
                }

                if (cmd.Price > 0)
                    postInDb.Price = cmd.Price;

                history.NewData = postInDb;
                history.ModifiedDate = DateTime.Now;
                postInDb.EditHistories.Add(history);
                _postRepository.UpdateOne(postId, postInDb);

                return new Resp<bool>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    true
                );

            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    false
                );
            }

        }

        public Resp<PagedList<PostResp>> GetPostByUserBookmark(string userId, Filter filter = null)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                List<PostDTO> postFromBookmarks = new List<PostDTO>();

                if (userInDb.Bookmarks is {Count: > 0})
                {
                    foreach (var postId in userInDb.Bookmarks)
                    {
                        var postInDb = _postRepository.GetById(postId);
                        if (postInDb != null)
                        {
                            postFromBookmarks.Add(postInDb);
                        }
                    }
                }
                var posts = RespExtensions<PostDTO>.ReflactorObject(postFromBookmarks, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter.PageNumber == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<PostResp>.ToPagedList(maps, 1, maps.Count()),
                        filter,
                        Paging.ToPaging<PostResp>(filter, maps)
                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );
                
            }
            catch (Exception e)
            {
                return new Resp<PagedList<PostResp>>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null, filter);
            }
        }

        public Resp<PagedList<PostResp>> GetPostByUserLike(string userId, Filter filter = null)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                List<PostDTO> postFromBookmarks = new List<PostDTO>();

                if (userInDb.LikedPosts is {Count: > 0})
                {
                    foreach (var postId in userInDb.LikedPosts)
                    {
                        var postInDb = _postRepository.GetById(postId);
                        if (postInDb != null)
                        {
                            postFromBookmarks.Add(postInDb);
                        }
                    }
                }
                var posts = RespExtensions<PostDTO>.ReflactorObject(postFromBookmarks, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter is {PageNumber: 0})
                {
                    return new Resp<PagedList<PostResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<PostResp>.ToPagedList(maps, 1, maps.Count()),
                        filter,
                        Paging.ToPaging<PostResp>(filter, maps)
                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );
                
            }
            catch (Exception e)
            {
                return new Resp<PagedList<PostResp>>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null, filter);
            }
        }

        public Resp<bool> BuyPost(string userId, string postId)
        {
            try
            {
                var user = _accountRepository.GetById(userId);
                if (user == null)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"Can not find the user corresponding to the id {userId}",
                        false);
                }

                var post = _postRepository.GetById(postId);
                if (post == null)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"Can not find the post corresponding to the id {postId}",
                        false);
                }

                if (post.Status != Enums.PostState.COMPLETE)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"The resource is now unavailable to buy",
                        false);
                }

                if (user.PaidPosts != null && user.PaidPosts.Contains(postId))
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"The post {postId} is already paid",
                        false);
                }
                

                string authorAddr = post.Author.WalletAddress;
                
                //make a payment
                var makeTrans = _userService.Transfer(user._id.ToString(), new CommandTransferCoin()
                {
                    Address = authorAddr,
                    Amount = post.Price
                }).Result;
                //todo

                if (!makeTrans.Payload)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        makeTrans.Message,
                        false);
                }
                
                //add to list
                if (user.PaidPosts == null)
                {
                    user.PaidPosts = new List<string>();
                }
                user.PaidPosts.Add(postId);

                _accountRepository.UpdateOne(userId, user);
                

                List<TransactionDTO> lstTrans = new List<TransactionDTO>();
                TransactionDTO src_trans = new TransactionDTO()
                {
                    TransactionAmount = post.Price,
                    TransactionType = ConstantCollection.TransactionType.PAY_PAYMENT,
                    FromWalletAddress = user.WalletAddress,
                    ToWalletAddress = post.Author.WalletAddress,
                    UserId = user._id.ToString(),
                    Description = $"Purchase post {post.Title}"
                };
                
                TransactionDTO dst_trans = new TransactionDTO()
                {
                    TransactionAmount = post.Price,
                    TransactionType = ConstantCollection.TransactionType.RECEIVED_PAYMENT,
                    FromWalletAddress = user.WalletAddress,
                    ToWalletAddress = post.Author.WalletAddress,
                    UserId = post.Author._id.ToString(),
                    Description = $"Received payment post {post.Title}"
                };
                lstTrans.Add(src_trans);
                lstTrans.Add(dst_trans);

                _transactionRepository.InsertManyAsync(lstTrans);

                return new Resp<bool>(
                    HttpStatusCode.OK,
                    String.Empty,
                    true);

            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    false);
            }
        }

        public Resp<PagedList<PostResp>> GetYourOwnPost(string userId, Filter filter)
        {
            try
            {
                var userInDb = _accountRepository.GetById(userId);
                List<PostDTO> postFromBookmarks = new List<PostDTO>();

                if (userInDb.PaidPosts is {Count: > 0})
                {
                    foreach (var postId in userInDb.PaidPosts)
                    {
                        var postInDb = _postRepository.GetById(postId);
                        if (postInDb != null)
                        {
                            postFromBookmarks.Add(postInDb);
                        }
                    }
                }
                var posts = RespExtensions<PostDTO>.ReflactorObject(postFromBookmarks, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                if (filter.PageNumber == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<PostResp>.ToPagedList(maps, 1, maps.Count()),
                        filter,
                        Paging.ToPaging<PostResp>(filter, maps)
                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );
                
            }
            catch (Exception e)
            {
                return new Resp<PagedList<PostResp>>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null, filter);
            }
        }

        public Resp<String> GetPostUrl(string userId, string postId)
        {
            try
            {
                var post = _postRepository.GetById(postId);
                if (post == null)
                {
                    return new Resp<String>(
                        HttpStatusCode.OK,
                        $"Can not find the post corresponding to the id {postId}",
                        String.Empty
                    );
                }

                var user = _accountRepository.GetById(userId);
                if (user == null)
                {
                    return new Resp<string>(
                        HttpStatusCode.OK,
                        $"Can not find the user corresponding to the id {userId}",
                        String.Empty);
                }

                if (post.Price == 0)
                {
                    return new Resp<string>(
                        HttpStatusCode.OK,
                        String.Empty,
                        post.Url);
                }

                if (user.Role == CustomAuthorize.LoginUser && userId != post.Author._id.ToString()&&!user.PaidPosts.Contains(postId))
                {
                    return new Resp<string>(
                        HttpStatusCode.OK,
                        $"You need to purchase the item",
                        String.Empty);
                }

                return new Resp<string>(
                    HttpStatusCode.OK,
                    String.Empty,
                    post.Url);
            }
            catch (Exception e)
            {
                return new Resp<string>(
                    HttpStatusCode.OK,
                    e.Message,
                    String.Empty);
            }
        }

        public Resp<PagedList<PostResp>> GetActivePosts(Filter filter = null)
        {
            try
            {
                var posts = _postRepository.GetActivePosts();
                posts = RespExtensions<PostDTO>.ReflactorObject(posts, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter.PageSize == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                                        System.Net.HttpStatusCode.OK,
                                        String.Empty,
                                        PagedList<PostResp>.ToPagedList(maps, 1, maps.Count()),
                                        filter,
                                        Paging.ToPaging<PostResp>(filter, maps)
                                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );

            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<PostResp>> GetApprovedPosts(Filter filter = null)
        {
            try
            {
                var posts = _postRepository.GetApprovedPosts();
                posts = RespExtensions<PostDTO>.ReflactorObject(posts, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts);
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter.PageSize == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                                        System.Net.HttpStatusCode.OK,
                                        String.Empty,
                                        PagedList<PostResp>.ToPagedList(maps, 1, maps.Count()),
                                        filter,
                                        Paging.ToPaging<PostResp>(filter, maps)
                                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );

            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PostResp> GetPost(string userId, string postId)
        {
            var postInDb = _postRepository.GetById(postId);

            if (postInDb == null)
                return new Resp<PostResp>(
                    System.Net.HttpStatusCode.BadRequest,
                    String.Format("Cannot find the post corresponding to the id {0}", postId),
                    null
                );

            // if (postInDb.Price != Decimal.Zero)
            // {
            //     var user = _accountRepository.GetById(userId);
            //     if (user.Role == CustomAuthorize.LoginUser)
            //     {
            //         
            //         if (!user.PaidPosts.Contains(postId) && postInDb.Author._id.ToString() != userId)
            //         {
            //             return new Resp<PostResp>(
            //                 HttpStatusCode.BadRequest,
            //                 $"You need to purchase the resource before using",
            //                 null);
            //         }
            //
            //         if (user._id.ToString() != postInDb.Author._id.ToString() && postInDb.Status != Enums.PostState.COMPLETE)
            //         {
            //             return new Resp<PostResp>(HttpStatusCode.BadRequest,
            //                 "The resource is now unavailable, will be comming soon",
            //                 null);
            //         }
            //     }
            // }
            
            var map = Mapping.Mapper.Map<PostResp>(postInDb);
            map.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, map.Id);
            return new Resp<PostResp>(
                System.Net.HttpStatusCode.OK,
                String.Empty,
                map);
        }

        public Resp<PagedList<PostResp>> GetPosts(Filter filter = null)
        {
            try
            {
                var posts = _postRepository.GetAll();
                posts = RespExtensions<PostDTO>.ReflactorObject(posts, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts);
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter.PageSize == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                                        System.Net.HttpStatusCode.OK,
                                        String.Empty,
                                        PagedList<PostResp>.ToPagedList(maps, 1, filter.PageSize),
                                        filter,
                                        Paging.ToPaging<PostResp>(filter, maps)
                                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );

            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<PostResp>> GetPostsByAuthorId(string authorId, Filter filter = null)
        {
            try
            {
                var posts = _postRepository.GetPostByAuthor(authorId);
                posts = RespExtensions<PostDTO>.ReflactorObject(posts, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter.PageSize == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                                        System.Net.HttpStatusCode.OK,
                                        String.Empty,
                                        PagedList<PostResp>.ToPagedList(maps, 1, filter.PageSize),
                                        filter,
                                        Paging.ToPaging<PostResp>(filter, maps)
                                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );

            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<PostResp>> GetPostsByCategoryId(string categoryId, Filter filter = null)
        {
            try
            {
                var posts = _postRepository.GetPostWithCategory(categoryId);
                posts = RespExtensions<PostDTO>.ReflactorObject(posts, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });
                if (filter.PageSize == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                                        System.Net.HttpStatusCode.OK,
                                        String.Empty,
                                        PagedList<PostResp>.ToPagedList(maps, 1, 1),
                                        filter,
                                        Paging.ToPaging<PostResp>(filter, maps)
                                    );
                }
                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<PostResp>(filter, maps)
                );

            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<PostResp>> GetPostsByResouce(string resourceId, Filter filter = null)
        {
            try
            {
                var posts = _postRepository.GetPostByResource(resourceId);
                posts = RespExtensions<PostDTO>.ReflactorObject(posts, filter);
                var maps = Mapping.Mapper.Map<IEnumerable<PostResp>>(posts).ToList();
                maps.ToList().ForEach(x =>
                {
                    x.ViewCount = _jedisMaster.GetViewCount(0, ConstantCollection.VIEW_COUNT_COLLECTION, x.Id);
                });

                if (filter.PageSize == 0)
                {
                    return new Resp<PagedList<PostResp>>(
                                        System.Net.HttpStatusCode.OK,
                                        String.Empty,
                                        PagedList<PostResp>.ToPagedList(maps, 1, 1),
                                        filter,
                                        Paging.ToPaging<PostResp>(filter, maps)
                                    );
                }
                else
                {
                    return new Resp<PagedList<PostResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<PostResp>.ToPagedList(maps, filter.PageNumber, filter.PageSize),
                        filter,
                        Paging.ToPaging<PostResp>(filter, maps)
                    );
                }



            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<PostResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }


        public async Task<Resp<bool>> LikePost(string userId, string postId, bool isLike = true)
        {
            try
            {
                var postInDb = await _postRepository.GetByIdAsync(postId);
                if (postInDb == null)
                    return new Resp<bool>(
                         System.Net.HttpStatusCode.BadRequest,
                         String.Format("Cannot find the post corresponding to the id {0}", postId),
                         false
                    );
                var accountDto = await _accountRepository.GetByIdAsync(userId);
                if (accountDto == null)
                    return new Resp<bool>(
                         System.Net.HttpStatusCode.BadRequest,
                         String.Format("Cannot find the user corresponding to the id {0}", userId),
                         false
                    );

                if (accountDto.LikedPosts == null)
                {
                    postInDb.LikeCount++;
                    accountDto.LikedPosts = new List<string>();
                    if (isLike)
                        accountDto.LikedPosts.Add(postId);
                }
                else
                {
                    if (isLike && !accountDto.LikedPosts.Any(x => x == postId))
                    {
                        accountDto.LikedPosts.Add(postId);
                        postInDb.LikeCount++;
                    }
                    if (!isLike && accountDto.LikedPosts.Any(x => x == postId))
                    {
                        accountDto.LikedPosts.Remove(postId);
                        postInDb.LikeCount--;
                    }

                }

                await _accountRepository.UpdateOneAsync(userId, accountDto);
                await _postRepository.UpdateOneAsync(postId, postInDb);
                return new Resp<bool>(
                     System.Net.HttpStatusCode.OK,
                     String.Empty,
                     true
                );
            }
            catch (System.Exception e)
            {

                return new Resp<bool>(
                     System.Net.HttpStatusCode.BadGateway,
                     e.Message,
                     false
                );
            }
        }

        public async Task<Resp<bool>> ReportPost(string postId, CommandReportPost cmd)
        {
            try
            {
                var postInDb = await _postRepository.GetByIdAsync(postId);
                if (postInDb == null)
                    return new Resp<bool>(
                         System.Net.HttpStatusCode.BadRequest,
                         String.Format("Cannot find the post corresponding to the id {0}", postId),
                         false
                    );
                var reporter = await _accountRepository.GetByIdAsync(cmd.ReporterId);

                if (reporter == null)
                    return new Resp<bool>(
                         System.Net.HttpStatusCode.BadRequest,
                         String.Format("Cannot find the account corresponding to the id {0}", cmd.ReporterId),
                         false
                    );

                ReportPostModel postReport = new ReportPostModel();
                postReport.Reason = cmd.Reason;
                postReport.Reporter = reporter;
                if (postInDb.Reports == null)
                {
                    postInDb.Reports = new List<ReportPostModel>();
                    postInDb.Reports.Add(postReport);
                }
                else
                {
                    postInDb.Reports.Add(postReport);
                }

                return new Resp<bool>(
                     System.Net.HttpStatusCode.OK,
                     String.Empty,
                     await _postRepository.UpdateOneAsync(postId, postInDb)
                );
            }
            catch (System.Exception e)
            {

                return new Resp<bool>(
                     System.Net.HttpStatusCode.BadGateway,
                     e.Message,
                     false
                );
            }
        }

        public async Task<Resp<bool>> ViewCountIncr(string postId)
        {
            try
            {
                var postInDb = await _postRepository.GetByIdAsync(postId);
                if (postInDb == null)
                    return new Resp<bool>(
                         System.Net.HttpStatusCode.BadRequest,
                         String.Format("Cannot find the post corresponding to the id {0}", postId),
                         false
                    );
                postInDb.ViewCount++;
                var map = Mapping.Mapper.Map<PostResp>(postInDb);
                await _jedisMaster.IncrHash(0, ConstantCollection.VIEW_COUNT_COLLECTION, postId);
                _rabbitMQ.Publish(QUEUE_UPDATE_POST, JsonConvert.SerializeObject(map));
                return new Resp<bool>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    true
                );

            }
            catch (System.Exception e)
            {

                return new Resp<bool>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    false
                );
            }
        }

        // public async Task<Resp<bool>> ViewCountIncr(string postId)
        // {
        //      var postInDb = await _postRepository.GetByIdAsync(postId);
        //      if (postInDb == null)
        //           return new Resp<bool>(
        //                System.Net.HttpStatusCode.BadRequest,
        //                String.Format("Cannot find the post corresponding to the id {0}", postId),
        //                false
        //           );
        //      _jedisMaster
        // }
    }
}