using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using net_core_blockchain_se_core.Domain.Post.Commands;
using net_core_blockchain_se_core.Domain.Post.Resps;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Post.Services
{
    public interface IPostService
    {
        Resp<PostResp> CreatePost(string authorId, CommandCreatePost cmd);
        Resp<PagedList<PostResp>> GetActivePosts(Filter filter = null);
        Resp<PagedList<PostResp>> GetApprovedPosts(Filter filter = null);
        Resp<PagedList<PostResp>> GetPosts(Filter filter = null);
        Resp<PostResp> GetPost(string userId,string postId);
        Resp<PagedList<PostResp>> GetPostsByCategoryId(string categoryId, Filter filter = null);
        Resp<PagedList<PostResp>> GetPostsByAuthorId(string authorId, Filter filter = null);
        Resp<PagedList<PostResp>> GetPostsByResouce(string resourceId, Filter filter = null);
        Resp<bool> ApprovePost(string postId, Decimal price=0);
        Task<Resp<bool>> ReportPost(string postId, CommandReportPost cmd);

        Task<Resp<bool>> LikePost(string userId, string postId, bool isLike = true);
        Task<Resp<bool>> ViewCountIncr(string postId);
        Resp<bool> EditPost(string userId, string postId, CommandEditPost cmd);
        Resp<PagedList<PostResp>> GetPostByUserBookmark(string userId, Filter filter = null);
        Resp<PagedList<PostResp>> GetPostByUserLike(string userId, Filter filter = null);
        Resp<bool> BuyPost(string userId, string postId);
        Resp<PagedList<PostResp>> GetYourOwnPost(string userId, Filter filter);
        Resp<String> GetPostUrl(string userId, string postId);
    }
}