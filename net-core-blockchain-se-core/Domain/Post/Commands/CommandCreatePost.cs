using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace net_core_blockchain_se_core.Domain.Post.Commands
{
    public class CommandCreatePost
    {
        [Required]
        public String Title { get; set; }
        [Required]
        public String Description { get; set; }
        [Required]
        public String CategoryId { get; set; }
        [Required]
        public String LevelId { get; set; }
        [Required]
        public String ResourceId { get; set; }
        public IFormFile Background { get; set; } = null;
        [Required]
        public string Source { get; set; }

        public decimal Price { get; set; } = 0;
    }
}