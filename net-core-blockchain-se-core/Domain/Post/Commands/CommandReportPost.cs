using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.Post.Commands
{
    public class CommandReportPost
    {
        [NotMappedAttribute]
        public String ReporterId { get; set; }
        [Required]
        public List<String> Reason { get; set; }
    }
}