using System;
using Microsoft.AspNetCore.Http;

namespace net_core_blockchain_se_core.Domain.Post.Commands
{
    public class CommandEditPost
    {
        public String Title { get; set; }
        public IFormFile Background { get; set; }
        public String Description { get; set; }
        public String CategoryId { get; set; }
        public String LevelId { get; set; }
        public String ResourceId { get; set; }
        public string Source { get; set; }
        public decimal Price { get; set; }
    }
}