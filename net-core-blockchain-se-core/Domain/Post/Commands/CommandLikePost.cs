using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.Post.Commands
{
    public class CommandLikePost
    {
        [Required]
        public string PostId { get; set; }
        public bool IsLike { get; set; }
    }
}