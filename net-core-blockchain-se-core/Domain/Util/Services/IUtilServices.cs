﻿using net_core_blockchain_se_core.Domain.Util.Commands;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Util.Services
{
    public interface IUtilServices
    {
        Resp<decimal> ConvertToElon(CommandRequestConversion cmd);
    }
}