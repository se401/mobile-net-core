﻿using System;
using System.Net;
using net_core_blockchain_se_core.Domain.Util.Commands;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Utility.Others;

namespace net_core_blockchain_se_core.Domain.Util.Services
{
    public class UtilServices : IUtilServices
    {
        
        public Resp<decimal> ConvertToElon(CommandRequestConversion cmd)
        {
            var result = ConvertUtil.Instance.Convert(cmd);
            if (!String.IsNullOrEmpty(result.Item2))
            {
                return new Resp<decimal>(HttpStatusCode.BadRequest, result.Item2, 0);
            }

            return new Resp<decimal>(HttpStatusCode.OK, String.Empty, result.Item1);
        }
    }
}