﻿using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.Util.Commands
{
    public class CommandRequestConversion
    {
        [Required]
        public string SourceCurrency { get; set; }
        [Required]
        public decimal Amount { get; set; }
    }
}