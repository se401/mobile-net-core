using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Level.Resps
{
    public class LevelResp:BaseResp
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}