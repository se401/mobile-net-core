using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.Level.Commands
{
    public class CommandCreateLevel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}