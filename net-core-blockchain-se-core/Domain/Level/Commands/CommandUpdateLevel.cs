namespace net_core_blockchain_se_core.Domain.Level.Commands
{
    public class CommandUpdateLevel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; } = true;
    }
}