using System;
using System.Collections.Generic;
using System.Linq;
using net_core_blockchain_se_core.Domain.Level.Commands;
using net_core_blockchain_se_core.Domain.Level.Resps;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Infra.Data.DTO.Level;
using net_core_blockchain_se_core.Infra.Repository.Level;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Utility.Mapper;

namespace net_core_blockchain_se_core.Domain.Level.Services
{
    public class LevelService : ILevelService
    {
        private readonly ILevelRepository _levelRepository;
        public LevelService(ILevelRepository levelRepository)
        {
            _levelRepository = levelRepository;
        }
        public Resp<LevelResp> CreateLevel(CommandCreateLevel cmd)
        {
            try
            {
                var level = new LevelDTO
                {
                    Name = cmd.Name,
                    Description = cmd.Description
                };

                _levelRepository.Insert(level);

                var map = Mapping.Mapper.Map<LevelResp>(level);
                return new Resp<LevelResp>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    map
                );
            }
            catch (System.Exception e)
            {

                return new Resp<LevelResp>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null
                );
            }
        }

        public Resp<PagedList<LevelResp>> GetActiveLevel(Filter filter = null)
        {
            try
            {
                var result = _levelRepository.GetActiveLevel();
                var map = Mapping.Mapper.Map<IEnumerable<LevelResp>>(result);

                map = RespExtensions<LevelResp>.ReflactorObject(map, filter);
                if (filter.PageNumber == 0)
                    return new Resp<PagedList<LevelResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<LevelResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<LevelResp>(filter,map)
                    );
                return new Resp<PagedList<LevelResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<LevelResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<LevelResp>(filter,map)
                );
            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<LevelResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<LevelResp>> GetAllLevel(Filter filter = null)
        {
            try
            {
                var result = _levelRepository.GetAll();
                var map = Mapping.Mapper.Map<IEnumerable<LevelResp>>(result);

                map = RespExtensions<LevelResp>.ReflactorObject(map, filter);

                if (filter.PageNumber == 0)
                    return new Resp<PagedList<LevelResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<LevelResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<LevelResp>(filter,map)
                    );
                return new Resp<PagedList<LevelResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<LevelResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<LevelResp>(filter,map)
                );
            }
            catch (System.Exception e)
            {

                return new Resp<PagedList<LevelResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<bool> UpdateLevel(string levelId, CommandUpdateLevel cmd)
        {
            try
            {
                var levelInDb = _levelRepository.GetById(levelId);
                if (levelInDb == null)
                    return new Resp<bool>(
                        System.Net.HttpStatusCode.BadRequest,
                        String.Format("Cannot find the corresponding to the id {0}", levelId),
                        false
                    );
                if (!String.IsNullOrEmpty(cmd.Name)) levelInDb.Name = cmd.Name;
                if (!String.IsNullOrEmpty(cmd.Description)) levelInDb.Description = cmd.Description;

                levelInDb.IsActive = cmd.IsActive;
                _levelRepository.UpdateOne(levelId, levelInDb);
                return new Resp<bool>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    true
                );
            }
            catch (System.Exception e)
            {

                return new Resp<bool>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    false
                );
            }
        }
    }
}