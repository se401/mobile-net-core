using System.Collections.Generic;
using net_core_blockchain_se_core.Domain.Level.Commands;
using net_core_blockchain_se_core.Domain.Level.Resps;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Level.Services
{
    public interface ILevelService
    {
         Resp<LevelResp> CreateLevel(CommandCreateLevel cmd);
         Resp<bool> UpdateLevel(string levelId, CommandUpdateLevel cmd);
         Resp<PagedList<LevelResp>> GetActiveLevel(Filter filter = null);
         Resp<PagedList<LevelResp>> GetAllLevel(Filter filter = null);
    }
}