﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Domain.Authentication.Commands
{
    public class CommandVerify
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string Code { get; set; }
    }
}
