﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Domain.Authentication.Commands
{
    public class CommandRegister
    {
        [Required]
        [EmailAddress(ErrorMessage ="Invalid email format")]
        public string Email { get; set; }
        [Required]
        [StringLength(maximumLength:64,ErrorMessage ="Password must be at least 8 to 64 characters",MinimumLength =8)]
        public string Password { get; set; }
        public string DisplayName { get; set; }
        [Required]
        [StringLength(maximumLength:16,MinimumLength =6)]
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
    }
}
