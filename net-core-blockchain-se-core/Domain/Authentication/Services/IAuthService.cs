﻿using net_core_blockchain_se_core.Domain.Authentication.Commands;
using net_core_blockchain_se_core.Domain.Authentication.Resps;
using net_core_blockchain_se_core.Infra.Data.DTO.User;
using net_core_blockchain_se_core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Domain.Authentication.Services
{
    public interface IAuthService
    {
        Resp<TokenResp> Login(CommandLogin cmdLogin);
        Resp<object> Register(CommandRegister cmdRegister);
        Resp<bool> Verify(CommandVerify cmdVerify);
    }
}
