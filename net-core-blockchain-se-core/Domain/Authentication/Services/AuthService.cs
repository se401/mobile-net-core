﻿using net_core_blockchain_se_core.Constant;
using net_core_blockchain_se_core.Domain.Authentication.Commands;
using net_core_blockchain_se_core.Domain.Authentication.Resps;
using net_core_blockchain_se_core.Helpers.Redis;
using net_core_blockchain_se_core.Infra.Data.DTO.User;
using net_core_blockchain_se_core.Infra.Repository.User;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Security.Brypt;
using net_core_blockchain_se_core.Security.Jwt;
using net_core_blockchain_se_core.Security.SHA256;
using net_core_blockchain_se_core.Utility.Others;
using net_core_blockchain_se_core.Utility.Sendgrid;
using System;
using System.Net;
using System.Threading.Tasks;
using net_core_blockchain_se_core.Helpers.Ethereum;
using net_core_blockchain_se_core.Helpers.Ethereum.EthModels;
using net_core_blockchain_se_core.Helpers.Http;

namespace net_core_blockchain_se_core.Domain.Authentication.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IBcryptMaster _bcryptMaster;
        private readonly IJedisMaster _jedisMaster;
        private readonly IJwtMaster _jwtMaster;
        private readonly IEthHelpers _ethHelpers;
        public AuthService(IAccountRepository accountRepository,
                            IBcryptMaster bcyptMaster,
                            IJedisMaster jedisMaster,
                            IJwtMaster jwtMaster,
                            IEthHelpers ethHelpers)
        {
            _accountRepository = accountRepository;
            _bcryptMaster = bcyptMaster;
            _jedisMaster = jedisMaster;
            _jwtMaster = jwtMaster;
            _ethHelpers = ethHelpers;

        }
        public Resp<TokenResp> Login(CommandLogin cmdLogin)
        {
            try
            {
                var account = _accountRepository.GetByEmail(cmdLogin.Email);

                if (account == null)
                    return new Resp<TokenResp>(System.Net.HttpStatusCode.BadRequest, String.Format("Email {0} is not exist", cmdLogin.Email), null);

                if (!_bcryptMaster.VerifyPassword(cmdLogin.Password, account.PasswordHash))
                    return new Resp<TokenResp>(System.Net.HttpStatusCode.BadRequest, String.Format("Pasword does not match"), null);

                if (!account.IsActive)
                    return new Resp<TokenResp>(System.Net.HttpStatusCode.BadRequest, "Account is not active", null);

                account.LastAccessedDate = DateTime.Now;
                _accountRepository.UpdateOneAsync(account._id.ToString(), account);
                TokenResp tokenResp = new TokenResp
                {
                    AccessToken = _jwtMaster.GenerateAccessToken(account._id.ToString(), account.Email, account.Role)
                };

                return new Resp<TokenResp>(System.Net.HttpStatusCode.OK, String.Empty, tokenResp);

            }
            catch (Exception e)
            {

                return new Resp<TokenResp>(System.Net.HttpStatusCode.BadRequest, e.ToString(), null);
            }
        }

        public Resp<object> Register(CommandRegister cmdRegister)
        {
            try
            {
                if (!String.IsNullOrEmpty(_jedisMaster.PullSpecificHash(0, ConstantCollection.EMAIL_COLLECTION, cmdRegister.Email)))
                    return new Resp<object>(System.Net.HttpStatusCode.BadRequest, String.Format("Email {0} has been taken", cmdRegister.Email), null);
                string hashPassword = _bcryptMaster.GenerateHash(cmdRegister.Password);

                var account = new AccountDTO
                {
                    Email = cmdRegister.Email,
                    PasswordHash = hashPassword,
                    VerificationCode = TextUtil.GenerateRandomCode(6),
                    Domain = nameof(AuthService),
                    Role = RoleCollection.User,
                    DisplayName = cmdRegister.DisplayName,
                    Username = cmdRegister.Username,
                    PhoneNumber =  cmdRegister.PhoneNumber,
                    IsActive = false
                };
                
                //CREATE ACCOUNT TO BLOCKCHAIN

                var addr =
                    _ethHelpers.CreateEthAccount(new CreateAccountModel(cmdRegister.Email, account.PasswordHash)).Result;

                if (addr == null)
                {
                    return new Resp<object>(HttpStatusCode.BadGateway, $"ETH: Unable to send the request", null);
                }

                if (!String.IsNullOrEmpty(addr.Error))
                {
                    return new Resp<object>(HttpStatusCode.BadGateway, $"ETH: {addr.Error}", null);
                }

                account.WalletAddress = addr.Address;

                _accountRepository.Insert(account);

                _jedisMaster.PushHashAsync(0, Constant.ConstantCollection.EMAIL_COLLECTION, account.Email, "1");

                string htmlContent = $"<text>Your verification code is: <strong>{account.VerificationCode}</strong></text> <br>";
                htmlContent += $"<text>The verification code will be expired in <strong>5 minutes</strong></text>";

                SendgridUtil.GetInstance().SendEmail(SendgridUtil.no_reply, account.Email, "Account Verification", htmlContent);

                string token = _bcryptMaster.GenerateHash(account._id.ToString());

                //save token to redis

                _jedisMaster.PushAsync(0, token, account._id.ToString(), TimeSpan.FromMinutes(5));

                return new Resp<object>(System.Net.HttpStatusCode.OK, String.Empty, new { tokenVerify = token });
            }
            catch (Exception ex)
            {

                return new Resp<object>(System.Net.HttpStatusCode.BadRequest, ex.Message, null);
            }

        }

        public Resp<bool> Verify(CommandVerify cmdVerify)
        {
            string dataStored = _jedisMaster.Pull(0, cmdVerify.Token);

            if (String.IsNullOrEmpty(dataStored))
                return new Resp<bool>(System.Net.HttpStatusCode.BadRequest, "Token is invalid", false);

            var accountInDb = _accountRepository.GetById(dataStored);

            if (accountInDb == null)
                return new Resp<bool>(System.Net.HttpStatusCode.BadRequest, "Account is not exist", false);

            if (cmdVerify.Code != accountInDb.VerificationCode)
                return new Resp<bool>(System.Net.HttpStatusCode.BadRequest, "Code does not match", false);

            accountInDb.IsActive = true;

            _accountRepository.UpdateOne(accountInDb._id.ToString(), accountInDb);

            return new Resp<bool>(System.Net.HttpStatusCode.OK, String.Empty, true);

        }

    }
}
