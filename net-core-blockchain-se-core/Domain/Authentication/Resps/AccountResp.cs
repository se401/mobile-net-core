﻿using net_core_blockchain_se_core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Domain.Authentication.Resps
{
    public class AccountResp:BaseResp
    {
        public string Email { get; set; }
        public string Role { get; set; }
        public DateTime LastAccessedDate { get; set; }
        public string DisplayName { get; set; }

    }
}
