﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Domain.Authentication.Resps
{
    public class TokenResp
    {
        public string AccessToken { get; set; }
    }
}
