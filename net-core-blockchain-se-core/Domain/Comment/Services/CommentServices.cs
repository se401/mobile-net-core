﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using net_core_blockchain_se_core.Domain.Comment.Commands;
using net_core_blockchain_se_core.Domain.Comment.Resp;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Infra.Data.DTO.Comment;
using net_core_blockchain_se_core.Infra.Repository.Comment;
using net_core_blockchain_se_core.Infra.Repository.Posts;
using net_core_blockchain_se_core.Infra.Repository.User;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Utility.Mapper;

namespace net_core_blockchain_se_core.Domain.Comment.Services
{
    public class CommentService:ICommentServices
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IPostRepository _postRepository;

        public CommentService(
            IAccountRepository accountRepository,
            ICommentRepository commentRepository,
            IPostRepository postRepository
        )
        {
            _accountRepository = accountRepository;
            _commentRepository = commentRepository;
            _postRepository = postRepository;
        }
        public Resp<CommentResp> CreateComment(string userId, CommandCreateComment cmd)
        {
            try
            {
                var postInDb = _postRepository.GetById(cmd.PostId);
                if (postInDb == null)
                    return new Resp<CommentResp>(
                        HttpStatusCode.BadRequest,
                        $"Cannot find the post corresponding to the id {cmd.PostId}",
                        null);

                var user = _accountRepository.GetById(userId);

                CommentDTO comment = new CommentDTO()
                {
                    Text = cmd.Message,
                    PostId = postInDb._id.ToString(),
                    User = user
                };

                if (!String.IsNullOrEmpty(cmd.ReplyCommentId))
                {
                    var commentFromDb = _commentRepository.GetById(cmd.ReplyCommentId);
                    if (commentFromDb != null)
                    {
                        comment.ReplyTo = commentFromDb;
                    }
                }

                _commentRepository.Insert(comment);

                var map = Mapping.Mapper.Map<CommentResp>(comment);
                return new Resp<CommentResp>(
                    HttpStatusCode.OK,
                    String.Empty,
                    map);
            }
            catch (Exception e)
            {
                return new Resp<CommentResp>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null);
            }
        }

        public Resp<bool> ReportComment(string userId, CommandReportComment cmd)
        {
            try
            {
                var commentInDb = _commentRepository.GetById(cmd.CommentId);
                if (commentInDb == null)
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"Cannot find the comment corresponding to the id {cmd.CommentId}", 
                        false);
                var accountInDb = _accountRepository.GetById(userId);
                commentInDb.Reports.Add(new Report()
                {
                    Reasons = cmd.Reasons,
                    User = accountInDb
                });

                _commentRepository.UpdateOne(cmd.CommentId, commentInDb);
                return new Resp<bool>(
                    HttpStatusCode.OK,
                    String.Empty,
                    true);
            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    false);
            }
        }

        public Resp<bool> EditCommet(string userId, CommandEditComment cmd)
        {
            try
            {
                var commentInDb = _commentRepository.GetById(cmd.CommentId);
                if (commentInDb == null)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"Cannot find the comment corresponding to the id {cmd.CommentId}",
                        false);
                }

                if (!commentInDb.User._id.ToString().Equals(userId))
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        $"The comment belongs to the other user",
                        false);
                }

                if (commentInDb.IsReported)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        "Your comment is being blocked",
                        false);
                }

                commentInDb.Text = cmd.Text;

                _commentRepository.UpdateOne(cmd.CommentId, commentInDb);

                return new Resp<bool>(
                    HttpStatusCode.OK,
                    String.Empty,
                    true);

            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    false);
            }
        }

        public Resp<bool> MarkCommentAsReported(string commentId)
        {
            try
            {
                var commentInDb = _commentRepository.GetById(commentId);

                if (commentInDb ==null)
                {
                    return new Resp<bool>(
                        HttpStatusCode.BadRequest,
                        String.Empty,
                        false);
                }

                commentInDb.IsReported = true;

                return new Resp<bool>(
                    HttpStatusCode.OK,
                    String.Empty,
                    true);
            }
            catch (Exception e)
            {
                return new Resp<bool>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    false);
            }
        }

        public Resp<PagedList<CommentResp>> GetCommentsByPost(string postId,Filter filter=null)
        {
            try
            {
                var comments = _commentRepository.GetActiveCommentByPost(postId);
                var map = Mapping.Mapper.Map<IEnumerable<CommentResp>>(comments);

                map = RespExtensions<CommentResp>.ReflactorObject(map, filter);
                if (filter.PageNumber == 0)
                    return new Resp<PagedList<CommentResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<CommentResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<CommentResp>(filter,map)
                    );
                return new Resp<PagedList<CommentResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<CommentResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<CommentResp>(filter,map)
                );
            }
            catch (Exception e)
            {
                return new Resp<PagedList<CommentResp>>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<CommentResp>> GetCommentsByUser(string userId, Filter filter = null)
        {
            try
            {
                var comments = _commentRepository.GetCommentByUser(userId);
                var map = Mapping.Mapper.Map<IEnumerable<CommentResp>>(comments);
                
                map = RespExtensions<CommentResp>.ReflactorObject(map, filter);
                
                if (String.IsNullOrEmpty(filter.SortField))
                {
                    map = map.OrderByDescending(x => x.CreatedDate);
                }
                
                if (filter.PageNumber == 0)
                    return new Resp<PagedList<CommentResp>>(
                        System.Net.HttpStatusCode.OK,
                        String.Empty,
                        PagedList<CommentResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<CommentResp>(filter,map)
                    );
                return new Resp<PagedList<CommentResp>>(
                    System.Net.HttpStatusCode.OK,
                    String.Empty,
                    PagedList<CommentResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<CommentResp>(filter,map)
                );
            }
            catch (Exception e)
            {
                return new Resp<PagedList<CommentResp>>(
                    HttpStatusCode.BadGateway,
                    e.Message,
                    null,
                    filter
                );
            }
        }
    }
}