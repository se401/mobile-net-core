﻿using System.Collections.Generic;
using net_core_blockchain_se_core.Domain.Comment.Commands;
using net_core_blockchain_se_core.Domain.Comment.Resp;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Comment.Services
{
    public interface ICommentServices
    {
        Resp<CommentResp> CreateComment(string userId, CommandCreateComment cmd);
        Resp<bool> ReportComment(string userId, CommandReportComment cmd);
        Resp<bool> EditCommet(string userId, CommandEditComment cmd);
        Resp<bool> MarkCommentAsReported(string commentId);
        Resp<PagedList<CommentResp>> GetCommentsByPost(string postId,Filter filter=null);
        Resp<PagedList<CommentResp>> GetCommentsByUser(string userId, Filter filter = null);
    }
}