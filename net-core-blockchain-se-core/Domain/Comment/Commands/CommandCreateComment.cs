﻿using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.Comment.Commands
{
    public class CommandCreateComment
    {
        [Required]
        public string Message { get; set; }
        public string ReplyCommentId { get; set; }
        [Required]
        public string PostId { get; set; }
    }
}