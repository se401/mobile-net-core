﻿using net_core_blockchain_se_core.Domain.Authentication.Resps;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Comment.Resp
{
    public class CommentResp:BaseResp
    {
        public string Text { get; set; }
        public CommentResp ReplyTo { get; set; }
        public string PostId { get; set; }
        public bool IsReported { get; set; }
        public string Reason { get; set; }
        public AccountResp User { get; set; }
    }
}