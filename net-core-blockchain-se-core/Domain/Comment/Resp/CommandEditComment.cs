﻿namespace net_core_blockchain_se_core.Domain.Comment.Resp
{
    public class CommandEditComment
    {
        public string CommentId { get; set; }
        public string Text { get; set; }
    }
}