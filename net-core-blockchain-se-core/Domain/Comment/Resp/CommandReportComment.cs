﻿using System.Collections.Generic;

namespace net_core_blockchain_se_core.Domain.Comment.Resp
{
    public class CommandReportComment
    {
        public string CommentId { get; set; }
        public List<string> Reasons { get; set; }
    }
}