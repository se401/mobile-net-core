using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Resource.Resps
{
    public class ResourceResp : BaseResp
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}