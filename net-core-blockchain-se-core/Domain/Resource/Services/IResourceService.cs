using System.Collections.Generic;
using net_core_blockchain_se_core.Domain.Resource.Commands;
using net_core_blockchain_se_core.Domain.Resource.Resps;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Domain.Resource.Services
{
    public interface IResourceService
    {
        Resp<ResourceResp> CreateResource(CommandCreateResource cmd);
        Resp<ResourceResp> UpdateResource(string resourceId, CommandUpdateResource cmd);
        Resp<PagedList<ResourceResp>> GetActiveResource(Filter filter = null);
        Resp<PagedList<ResourceResp>> GetAllResource(Filter filter = null);
    }
}