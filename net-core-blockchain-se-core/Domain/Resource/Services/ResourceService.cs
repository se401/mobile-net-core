using System.Collections.Generic;
using System.Linq;
using net_core_blockchain_se_core.Domain.Resource.Commands;
using net_core_blockchain_se_core.Domain.Resource.Resps;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Infra.Data.DTO.Resource;
using net_core_blockchain_se_core.Infra.Repository.Resource;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Utility.Mapper;

namespace net_core_blockchain_se_core.Domain.Resource.Services
{
    public class ResourceService : IResourceService
    {
        private readonly IResourceRepository _resourceRepository;
        public ResourceService(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
        }
        public Resp<ResourceResp> CreateResource(CommandCreateResource cmd)
        {
            try
            {
                var resourceDTO = new ResourceDTO
                {
                    Name = cmd.Name,
                    Description = cmd.Description
                };

                _resourceRepository.Insert(resourceDTO);
                var map = Mapping.Mapper.Map<ResourceResp>(resourceDTO);
                return new Resp<ResourceResp>(
                    System.Net.HttpStatusCode.OK,
                    string.Empty,
                    map
                );
            }
            catch (System.Exception e)
            {

                return new Resp<ResourceResp>(
                    System.Net.HttpStatusCode.BadGateway,
                    e.Message,
                    null
                );
            }
        }

        public Resp<PagedList<ResourceResp>> GetActiveResource(Filter filter = null)
        {
            try
            {
                var activeResources = _resourceRepository.GetActiveResource();

                var map = Mapping.Mapper.Map<IEnumerable<ResourceResp>>(activeResources);

                map = RespExtensions<ResourceResp>.ReflactorObject(map, filter);
                if (filter.PageSize > 0)
                    return new Resp<PagedList<ResourceResp>>(
                    System.Net.HttpStatusCode.OK,
                    string.Empty,
                    PagedList<ResourceResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<ResourceResp>(filter, map)
                );
                return new Resp<PagedList<ResourceResp>>(
                    System.Net.HttpStatusCode.OK,
                    string.Empty,
                    PagedList<ResourceResp>.ToPagedList(map, 1, map.Count()),
                    filter,
                    Paging.ToPaging<ResourceResp>(filter, map)
                );
            }
            catch (System.Exception ex)
            {
                return new Resp<PagedList<ResourceResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    ex.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<PagedList<ResourceResp>> GetAllResource(Filter filter = null)
        {
            try
            {
                var activeResources = _resourceRepository.GetAll();

                var map = Mapping.Mapper.Map<IEnumerable<ResourceResp>>(activeResources);

                map = RespExtensions<ResourceResp>.ReflactorObject(map, filter);

                var count = map.Count();
                if (filter.PageSize == 0)
                    return new Resp<PagedList<ResourceResp>>(
                        System.Net.HttpStatusCode.OK,
                        string.Empty,
                        PagedList<ResourceResp>.ToPagedList(map, 1, map.Count()),
                        filter,
                        Paging.ToPaging<ResourceResp>(filter, map)
                    );
                return new Resp<PagedList<ResourceResp>>(
                    System.Net.HttpStatusCode.OK,
                    string.Empty,
                    PagedList<ResourceResp>.ToPagedList(map, filter.PageNumber, filter.PageSize),
                    filter,
                    Paging.ToPaging<ResourceResp>(filter, map)
                );
            }
            catch (System.Exception ex)
            {

                return new Resp<PagedList<ResourceResp>>(
                    System.Net.HttpStatusCode.BadGateway,
                    ex.Message,
                    null,
                    filter
                );
            }
        }

        public Resp<ResourceResp> UpdateResource(string resourceId, CommandUpdateResource cmd)
        {
            try
            {
                var resourceInDb = _resourceRepository.GetById(resourceId);
                if (resourceInDb == null)
                    return new Resp<ResourceResp>(
                        System.Net.HttpStatusCode.BadRequest,
                        string.Format("Cannot find the resource corresponding to the id {0}", resourceId),
                        null
                    );
                if (!string.IsNullOrEmpty(cmd.Name))
                    resourceInDb.Name = cmd.Name;
                if (!string.IsNullOrEmpty(cmd.Description))
                    resourceInDb.Description = cmd.Description;
                resourceInDb.IsActive = cmd.IsActive;
                _resourceRepository.UpdateOne(resourceId, resourceInDb);
                var map = Mapping.Mapper.Map<ResourceResp>(resourceInDb);
                return new Resp<ResourceResp>(
                    System.Net.HttpStatusCode.OK,
                    string.Empty,
                    map
                );
            }
            catch (System.Exception ex)
            {

                return new Resp<ResourceResp>(
                    System.Net.HttpStatusCode.BadRequest,
                    ex.Message,
                    null
                );
            }
        }
    }
}