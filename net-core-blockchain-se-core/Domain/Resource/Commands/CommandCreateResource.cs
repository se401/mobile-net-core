using System.ComponentModel.DataAnnotations;

namespace net_core_blockchain_se_core.Domain.Resource.Commands
{
    public class CommandCreateResource
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}