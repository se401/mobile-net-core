namespace net_core_blockchain_se_core.Domain.Resource.Commands
{
    public class CommandUpdateResource
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }=true;
    }
}