﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace net_core_blockchain_se_core.Security.SHA256
{
    public class SHA256Master
    {
        private static SHA256Master instance;
        public static SHA256Master GetInstance()
        {
            if (instance == null)
                instance = new SHA256Master();
            return instance;
        }
        private SHA256Master()
        {

        }

        public string GenerateSHA256Hash(string rawString)
        {
            using (HMACSHA256 sha256Hash = new HMACSHA256())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawString));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
