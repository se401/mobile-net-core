﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Security.Jwt
{
    public interface IJwtMaster
    {
        public string GenerateAccessToken(string uuid, string email, string role);
        public string GenerateRefreshToken(string uuid);
        public IDictionary<string,object> VerifyToken(string token);
    }
}
