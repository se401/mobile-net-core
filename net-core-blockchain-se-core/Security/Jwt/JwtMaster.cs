﻿
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Security.Jwt
{
    public class JwtMaster : IJwtMaster
    {
        private readonly JwtSetting jwtSetting;
        public JwtMaster()
        {
            jwtSetting = new JwtSetting();
            var rootConfiguration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            jwtSetting.SecrectKey = rootConfiguration.GetSection("Jwt:SecrectKey").Value.ToString();

            jwtSetting.Expired = Convert.ToInt32(rootConfiguration.GetSection("Jwt:Expired").Value.ToString());

        }
        public string GenerateAccessToken(string uuid, string email, string role)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtSetting.SecrectKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name,uuid),
                    new Claim(ClaimTypes.NameIdentifier,uuid),
                    new Claim(ClaimTypes.Role,role),
                    new Claim(JwtRegisteredClaimNames.Sub, email),
                    new Claim(JwtRegisteredClaimNames.Email,email),
                }),
                Expires = DateTime.Now.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                //Audience = "http://localhost:64645",
                //Issuer = "http://localhost:64645"

            };

            var token = jwtTokenHandler.CreateToken(tokenDescriptor);

            var jwtToken = jwtTokenHandler.WriteToken(token);

            return jwtToken;
        }

        public string GenerateRefreshToken(string uuid)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtSetting.SecrectKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new[]
                {
                    new Claim("Id", uuid),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                }),
                Expires = DateTime.Now.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescriptor);

            return jwtTokenHandler.WriteToken(token);
        }

        public IDictionary<string, object> VerifyToken(string token)
        {
            try
            {
                var payload = new JwtBuilder().
                                WithAlgorithm(new HMACSHA256Algorithm())
                                .WithSecret(jwtSetting.SecrectKey)
                                .MustVerifySignature()
                                .Decode<IDictionary<string, object>>(token);
                return payload;
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
