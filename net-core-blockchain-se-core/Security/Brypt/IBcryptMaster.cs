﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Security.Brypt
{
    public interface IBcryptMaster
    {
        public string GenerateHash(string password);
        public bool VerifyPassword(string password, string hash);
        public string GenerateSalt();
    }
}
