﻿using BCrypt.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Security.Brypt
{
    public class BcryptMaster : IBcryptMaster
    {
        private readonly string privateKey = "$TNKyfMatYLXqp49YrdSZp2d02XI!xtko(fx3D3&fX06J!CONeuc96w0&o1p@3!#";
        private readonly string salt;
        public BcryptMaster()
        {
            salt = BCrypt.Net.BCrypt.GenerateSalt();
        }
        public string GenerateHash(string password)
        {
            string completePassword = password + privateKey;
            return BCrypt.Net.BCrypt.HashPassword(completePassword, salt,true,HashType.SHA256);
        }

        public string GenerateSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt();
        }

        public bool VerifyPassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password + privateKey, hash, true, HashType.SHA256);
        }

    }
}
