﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace net_core_blockchain_se_core.Security.MD5Sec
{
    public class MD5Master
    {
        private readonly string privateKey = "$TNKyfMatYLXqp49YrdSZp2d02XI!xtko(fx3D3&fX06J!CONeuc96w0&o1p@3!#";

        private readonly MD5 md5;

        private static MD5Master instance;
        public static MD5Master GetInstance()
        {
            if (instance == null)
                instance = new MD5Master();
            return instance;
        }

        public MD5Master()
        {
            md5 = new MD5CryptoServiceProvider();
        }

        public String GenerateHash(string text)
        {
            string completeText = text + privateKey;

            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(completeText));

            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
}
