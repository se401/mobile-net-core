using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using MongoDB.Bson;
using MongoDB.Driver;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Extensions
{
    public static class FilterExtensions
    {
        public static IEnumerable<T> Filter<T>(this IEnumerable<T> source, Filter filter)
        {
            var query = source.AsQueryable();
            if (!String.IsNullOrEmpty(filter.FilterField))
            {
                string filterStr = BuildFilterString<T>(filter);
                return query.Where(filterStr, filter.FilterValue.ToLower()); ;
            }
            return source;
        }
        private static string BuildFilterString<T>(Filter filter)
        {
            if (String.IsNullOrEmpty(filter.FilterValue))
                return $"{filter.FilterField} IS NOT NULL";
            if (filter.FilterOperator == "startswith" || filter.FilterOperator == "endswith" || filter.FilterOperator == "contains")
                return $"{filter.FilterField}.ToLower().{Operators[filter.FilterOperator]}(@0)";
            return $"{filter.FilterField} {Operators[filter.FilterOperator]} @0";
        }

        private static readonly IDictionary<string, string>
        Operators = new Dictionary<string, string>
        {
        {"eq", "="},
        {"neq", "!="},
        {"lt", "<"},
        {"lte", "<="},
        {"gt", ">"},
        {"gte", ">="},
        {"startswith", "StartsWith"},
        {"endswith", "EndsWith"},
        {"contains", "Contains"},
        };

    }
}