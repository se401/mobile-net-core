using System.Collections.Generic;
using net_core_blockchain_se_core.Model;

namespace net_core_blockchain_se_core.Extensions
{
    public static class RespExtensions<T>
    {
        public static IEnumerable<T> ReflactorObject(IEnumerable<T> source,Filter filter){
            source = FilterExtensions.Filter<T>(source,filter);
            source = SortExtensions.Sort<T>(source,filter.SortField,filter.SortOrder);
            return source;
        }
    }
}