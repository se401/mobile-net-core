using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using net_core_blockchain_se_core.Domain.Authentication.Services;
using net_core_blockchain_se_core.Domain.Category.Services;
using net_core_blockchain_se_core.Domain.Level.Services;
using net_core_blockchain_se_core.Domain.Post.Services;
using net_core_blockchain_se_core.Domain.Resource.Services;
using net_core_blockchain_se_core.Domain.User.Services;
using net_core_blockchain_se_core.Helpers.RabbitMQ;
using net_core_blockchain_se_core.Helpers.Redis;
using net_core_blockchain_se_core.Infra.Repository.Category;
using net_core_blockchain_se_core.Infra.Repository.Level;
using net_core_blockchain_se_core.Infra.Repository.Posts;
using net_core_blockchain_se_core.Infra.Repository.Resource;
using net_core_blockchain_se_core.Infra.Repository.User;
using net_core_blockchain_se_core.Security.Brypt;
using net_core_blockchain_se_core.Security.Jwt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using net_core_blockchain_se_core.Domain.Comment.Services;
using net_core_blockchain_se_core.Domain.Util.Services;
using net_core_blockchain_se_core.Helpers.Ethereum;
using net_core_blockchain_se_core.Helpers.Firebase;
using net_core_blockchain_se_core.Helpers.Http;
using net_core_blockchain_se_core.Infra.Repository.Comment;
using net_core_blockchain_se_core.Infra.Repository.Transaction;

namespace net_core_mobile_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            string key = Configuration.GetSection("Jwt:SecrectKey").Value.ToString();


            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(opt =>
                {
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        //ValidIssuer = "http://localhost:64645",
                        //ValidAudience = "http://localhost:64645",
                        //ValidateIssuer = true,
                        //ValidateAudience = true,

                    };
                    opt.SaveToken = true;
                    opt.RequireHttpsMetadata = false;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "net_core_mobile_api", Version = "v1" });
            });

            services.AddHttpClient();
            
            RegisterService(services);

            services.AddCors();
        }

        private void RegisterService(IServiceCollection services)
        {
            #region  repositories
            services.AddSingleton<IAccountRepository, AccountRepository>();
            services.AddSingleton<ICategoryRepository, CategoryRepository>();
            services.AddSingleton<IResourceRepository, ResourceRepository>();
            services.AddSingleton<ILevelRepository, LevelRepository>();
            services.AddSingleton<IPostRepository, PostRepository>();
            services.AddSingleton<ICommentRepository, CommentRepository>();
            services.AddSingleton<ITransactionRepository, TransactionRepository>();
            #endregion

            #region Services
            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<ICategoryService, CategoryService>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IResourceService, ResourceService>();
            services.AddSingleton<ILevelService, LevelService>();
            services.AddSingleton<IPostService, PostService>();
            services.AddSingleton<IFirebaseMaster, FirebaseMaster>();
            services.AddSingleton<ICommentServices, CommentService>();
            services.AddSingleton<IUtilServices,UtilServices>();
            #endregion

            #region Helpers
            services.AddSingleton<IJedisMaster, JedisMaster>();
            services.AddSingleton<IJwtMaster, JwtMaster>();
            services.AddSingleton<IBcryptMaster, BcryptMaster>();
            services.AddSingleton<IAQMPMaster, AQMPMaster>();
            services.AddSingleton<IHttpHelpers, HttpHelpers>();
            services.AddSingleton<IEthHelpers, EthHelpers>();
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "net_core_mobile_api v1"));
            }

            app.UseRouting();

            app.UseCors(options => { options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            app.UseAuthorization();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "/{action=swagger}");
            });
        }
    }
}
