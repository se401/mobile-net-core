using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Level.Commands;
using net_core_blockchain_se_core.Domain.Level.Services;
using net_core_blockchain_se_core.Model;
using static net_core_blockchain_se_core.Middleware.CustomAuthorize;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LevelController : ControllerBase
    {
        private readonly ILevelService _levelService;
        public LevelController(ILevelService levelService)
        {
            _levelService = levelService;
        }

        [Route("health")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult Health()
        {
            var user = User;
            return Ok();
        }

        [Route("get-levels")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult GetLevels([FromQuery] Filter filter)
        {
            var result = _levelService.GetAllLevel(filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("create-level")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult CreateLevel([FromBody] CommandCreateLevel cmd)
        {
            var result = _levelService.CreateLevel(cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("update-level/{levelId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult UpdateLevel(string levelId, [FromBody] CommandUpdateLevel cmd)
        {
            var result = _levelService.UpdateLevel(levelId, cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-active-levels")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetActiveLevel([FromQuery] Filter filter)
        {
            var result = _levelService.GetActiveLevel(filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
    }
}