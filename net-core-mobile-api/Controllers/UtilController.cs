﻿using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Util.Commands;
using net_core_blockchain_se_core.Domain.Util.Services;

namespace net_core_mobile_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UtilController : ControllerBase
    {
        private readonly IUtilServices _utilServices;

        public UtilController(IUtilServices utilServices)
        {
            _utilServices = utilServices;
        }
        [HttpPost]
        [Route("convert-currency")]
        public IActionResult ConvertCurrency([FromBody] CommandRequestConversion cmd)
        {
            var result = _utilServices.ConvertToElon(cmd);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
    }
}