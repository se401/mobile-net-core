using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Post.Commands;
using net_core_blockchain_se_core.Domain.Post.Services;
using net_core_blockchain_se_core.Model;
using static net_core_blockchain_se_core.Middleware.CustomAuthorize;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [Route("create-post")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult CreatePost([FromForm] CommandCreatePost cmd)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.CreatePost(userId, cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-post/{postId}")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetPost(string postId)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.GetPost(userId,postId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-posts-cate/{categoryId}")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetPostWithCategory(string categoryId, [FromQuery]Filter filter)
        {
            var result = _postService.GetPostsByCategoryId(categoryId,filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-posts-author/{authorId}")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetPostWithAuthor(string authorId, [FromQuery]Filter filter)
        {
            var result = _postService.GetPostsByAuthorId(authorId,filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-posts-resource/{resourceId}")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetPostWithResource(string resourceId, [FromQuery] Filter filter)
        {
            var result = _postService.GetPostsByResouce(resourceId, filter);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }


        [Route("get-active-posts")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetActivePosts([FromQuery] Filter filter)
        {
            var result = _postService.GetActivePosts(filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-all-posts")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult GetAll([FromQuery] Filter filter)
        {
            var result = _postService.GetPosts(filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-appr-posts")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetApprovedPost([FromQuery] Filter filter)
        {
            var result = _postService.GetApprovedPosts(filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("approve/{postId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult ApprovePost(string postId, [FromForm]Decimal price)
        {

            var result = _postService.ApprovePost(postId,price);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("report/{postId}")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public async Task<IActionResult> ReportPost(string postId, [FromBody] CommandReportPost cmd)
        {

            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            cmd.ReporterId = userId;
            var result = await _postService.ReportPost(postId, cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("like")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public async Task<IActionResult> LikePost([FromBody] CommandLikePost cmd)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = await _postService.LikePost(userId, cmd.PostId, cmd.IsLike);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("view/{postId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public async Task<IActionResult> ViewPost(string postId)
        {
            var result = await _postService.ViewCountIncr(postId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        [Route("edit/{postId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult EditPost(string postId, [FromForm] CommandEditPost cmd)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.EditPost(userId, postId, cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-bookmark-post")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetBookmarks([FromQuery] Filter filter = null)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.GetPostByUserBookmark(userId, filter);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
        
        [Route("get-liked-posts")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetLikedPosts([FromQuery] Filter filter = null)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.GetPostByUserLike(userId, filter);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
        
        
        [Route("buy/{postId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult BuyPost(string postId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.BuyPost(userId, postId);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
        
        [Route("get-url/{postId}")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyLoginUser]
        public IActionResult GetUrl(string postId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _postService.GetPostUrl(userId, postId);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
    }
}