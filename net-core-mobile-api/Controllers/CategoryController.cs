using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Category.Commands;
using net_core_blockchain_se_core.Domain.Category.Resps;
using net_core_blockchain_se_core.Domain.Category.Services;
using net_core_blockchain_se_core.Extensions;
using net_core_blockchain_se_core.Model;
using static net_core_blockchain_se_core.Middleware.CustomAuthorize;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Route("health")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult Health()
        {
            var user = User;
            return Ok();
        }


        [Route("create-category")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult CreateCategory([FromForm] CommandCreateCategory command, IFormFile file)
        {
            var result = _categoryService.CreateCategory(command, file);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("update-category/{categoryId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult UpdateCateogry(string categoryId, [FromForm] CommandUpdateCategory command, IFormFile file)
        {
            var result = _categoryService.UpdateCategory(categoryId, command, file);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-categories")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult GetAllCategories([FromQuery] Filter cmdFilter)
        {
            var result = _categoryService.GetAllCategories(cmdFilter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-active-categories")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [AllowAnonymous]
        public IActionResult GetActiveCategories([FromQuery] Filter cmdFilter)
        {
            var result = _categoryService.GetActiveCategories(cmdFilter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
    }
}