using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.User.Commands;
using net_core_blockchain_se_core.Domain.User.Services;
using net_core_blockchain_se_core.Helpers.Ethereum.EthModels;
using net_core_blockchain_se_core.Model;
using net_core_blockchain_se_core.Security.Brypt;
using static net_core_blockchain_se_core.Middleware.CustomAuthorize;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IBcryptMaster _bcryptMaster;

        public UserController(IUserService userService,
                              IBcryptMaster bcryptMaster)
        {
            _userService = userService;
            _bcryptMaster = bcryptMaster;
        }

        [HttpPost]
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("update-account-info")]
        public IActionResult UpdateAccountInfo([FromBody] CommandUpdateUserInfo command)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.UpdateUserInfo(userId, command);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }


        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("change-avatar")]
        [HttpPost]
        public IActionResult ChangeUserAvatar(IFormFile file)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.ChangeAvatar(userId, file);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("change-password")]
        [HttpPost]
        public IActionResult ChangePassword([FromBody] CommandChangePassword cmd)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.ChangePassword(userId,cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [OnlyAdmin]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("get-users")]
        [HttpGet]
        public IActionResult GetUser()
        {
            var result = _userService.GetUsers();
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [OnlyAdmin]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("admin/get-user/{userId}")]
        [HttpGet]
        public IActionResult AdminGetUserInfo(string userId)
        {
            var result = _userService.GetUserInfo(userId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("get-user-info")]
        [HttpGet]
        public IActionResult GetUserInfo()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.GetUserInfo(userId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("save-bookmark/{postId}")]
        [HttpPost]
        public IActionResult SaveBookmark(string postId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.SaveBookmark(userId,postId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("remove-bookmark/{postId}")]
        [HttpPost]
        public IActionResult RemoveBookmark(string postId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.RemoveBookmark(userId,postId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("get-eth-balance")]
        [HttpGet]
        public async Task<IActionResult> GetBalance(string postId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = await _userService.GetBalance(userId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("gen-eth-api-key")]
        [HttpPost]
        public async Task<IActionResult> GenApiKey([FromBody]CreateApiKey model)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = await _userService.CreateApiKey(userId,model);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("transfer")]
        [HttpPost]
        public async Task<IActionResult> GenApiKey([FromBody]CommandTransferCoin cmd)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = await _userService.Transfer(userId,cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("get-transactions")]
        [HttpGet]
        public async Task<IActionResult> GetTransactions([FromQuery]Filter filter = null)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result =  _userService.GetTransactions(userId,filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("get-token-reward")]
        [HttpGet]
        public async Task<IActionResult> GetTokenReward()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result =  _userService.GetTokenReward(userId);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
        
        [OnlyLoginUser]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("reward")]
        [HttpPost]
        public IActionResult Reward([FromBody] CommandReward cmd)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _userService.Reward(userId, cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
    }
}