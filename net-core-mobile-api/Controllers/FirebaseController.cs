﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Helpers.Firebase;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FirebaseController : ControllerBase
    {
        private readonly IFirebaseMaster _firebaseMaster;
        public FirebaseController(IFirebaseMaster firebaseMaster)
        {
            _firebaseMaster = firebaseMaster;
        }

        [Route("test")]
        [HttpPost]
        public async Task<IActionResult> PushNotify()
        {
            await  _firebaseMaster.SendNotification(null, "test", "ahsdfjkhakjsdhfkajshf");
            return Ok();
        }
    }
}