﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Comment.Commands;
using net_core_blockchain_se_core.Domain.Comment.Resp;
using net_core_blockchain_se_core.Domain.Comment.Services;
using net_core_blockchain_se_core.Middleware;
using net_core_blockchain_se_core.Model;

namespace net_core_mobile_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentServices _commentServices;

        public CommentController(ICommentServices commentServices)
        {
            _commentServices = commentServices;
        }

        [HttpPost]
        [Route("create")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CustomAuthorize.OnlyLoginUser]
        public IActionResult CreateComment([FromBody] CommandCreateComment cmd)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _commentServices.CreateComment(userId, cmd);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }

        [HttpPost]
        [Route("edit")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CustomAuthorize.OnlyLoginUser]
        public IActionResult EditComment([FromBody] CommandEditComment cmd)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _commentServices.EditCommet(userId, cmd);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }

        [HttpGet]
        [Route("get-comments/{postid}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CustomAuthorize.OnlyLoginUser]
        public IActionResult GetComments(string postid, [FromQuery] Filter filter)
        {
            var result = _commentServices.GetCommentsByPost(postid, filter);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }

        [HttpPost]
        [Route("report-comment")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CustomAuthorize.OnlyLoginUser]
        public IActionResult Report([FromBody] CommandReportComment cmd)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _commentServices.ReportComment(userId, cmd);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }

        [HttpPost]
        [Route("mark-as-blocked/{commentId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CustomAuthorize.OnlyAdmin]
        public IActionResult MarkCommentAsReported(string commentId)
        {
            var result = _commentServices.MarkCommentAsReported(commentId);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
        
        [HttpGet]
        [Route("get-user-comments")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CustomAuthorize.OnlyLoginUser]
        public IActionResult GetUserComments([FromQuery] Filter filter)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
            var result = _commentServices.GetCommentsByPost(userId, filter);
            Response.StatusCode = (int) result.StatusCode;
            return new JsonResult(result);
        }
    }
}