﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Authentication.Commands;
using net_core_blockchain_se_core.Domain.Authentication.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static net_core_blockchain_se_core.Middleware.CustomAuthorize;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }


        [HttpPost]
        [Route("register")]
        public IActionResult Register([FromBody] CommandRegister commandRegister)
        {
            var result = _authService.Register(commandRegister);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] CommandLogin cmdLogin)
        {
            var result = _authService.Login(cmdLogin);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [HttpPost]
        [Route("verify")]
        public IActionResult Veriy([FromBody] CommandVerify commandVerify)
        {
            var result = _authService.Verify(commandVerify);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
    }
}
