using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using net_core_blockchain_se_core.Domain.Resource.Commands;
using net_core_blockchain_se_core.Domain.Resource.Services;
using net_core_blockchain_se_core.Model;
using static net_core_blockchain_se_core.Middleware.CustomAuthorize;

namespace net_core_mobile_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResourceController : ControllerBase
    {
        private readonly IResourceService _resourceService;
        public ResourceController(IResourceService resourceService)
        {
            _resourceService = resourceService;
        }


        [Route("health")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult Health()
        {
            var user = User;
            return Ok();
        }

        [Route("get-resources")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult GetResources([FromQuery] Filter cmdFilter = null)
        {

            var result = _resourceService.GetAllResource(cmdFilter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("create-resource")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult CreateResource([FromBody] CommandCreateResource cmd)
        {
            var result = _resourceService.CreateResource(cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("update-resource/{resourceId}")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [OnlyAdmin]
        public IActionResult UpdateResource(string resourceId, [FromBody] CommandUpdateResource cmd)
        {
            var result = _resourceService.UpdateResource(resourceId, cmd);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }

        [Route("get-active-resources")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetActiveResource([FromQuery]Filter filter = null)
        {
            var result = _resourceService.GetActiveResource(filter);
            Response.StatusCode = (int)result.StatusCode;
            return new JsonResult(result);
        }
    }
}